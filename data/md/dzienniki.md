DZIENNIK USTAW 
RZECZYPOSPOLITEJ POLSKIEJ 

Warszawa, dnia 2 marca 2021 r. 

Poz. 381 

RO ZPO RZĄDZENIE  

PREZESA RADY M INISTR Ó W 

z dnia 22 lutego 2021 r. 

w sprawie trybu przeprowadzania postępowania kwalifikacyjnego oraz uzupełniającego postępowania kwalifikacyjnego 
dla kandydatów na członków Krajowej Izby Odwoławczej 

Na  podstawie  art. 477 ust. 10  ustawy  z dnia  11 września  2019 r.  –  Prawo  zamówień  publicznych  (Dz. U.  poz. 2019 

oraz z 2020 r. poz. 288, 1492, 1517, 2275 i 2320) zarządza się, co następuje: 

Rozdział 1 

Przepisy ogólne 

§ 1. Rozporządzenie określa: 

1) 

2) 

3) 

4) 

tryb  przeprowadzania  postępowania  kwalifikacyjnego  oraz  uzupełniającego  postępowania  kwalifikacyjnego,  a także 
sposób  ustalania  jego  wyniku,  jak również  sposób wniesienia odwołania od wyniku oraz tryb i sposób  rozpatrzenia 
odwołania; 

dokumenty,  które  należy  dołączyć  do  zgłoszenia  kandydata  na  członka  Izby,  potwierdzające  spełnianie  warunków, 
o których mowa w art. 474 ust. 2 pkt 1–6 i 8–10 ustawy z dnia 11 września 2019 r. – Prawo zamówień publicznych, 
oraz zakres danych, które ma zawierać to zgłoszenie; 

szczegółowy zakres zagadnień, w oparciu o które przeprowadzane jest postępowanie kwalifikacyjne oraz uzupełnia-
jące postępowanie kwalifikacyjne; 

sposób powoływania komisji kwalifikacyjnej, szczegółowe wymagania wobec członków komisji kwalifikacyjnej oraz 
organizację jej pracy. 

Zgłoszenie kandydata na członka Krajowej Izby Odwoławczej 

Rozdział 2 

§ 2. 1. Zgłoszenie kandydata na członka Krajowej Izby Odwoławczej, zwanej dalej „Izbą”, jest składane w postępo-
waniu kwalifikacyjnym, w miejscu wskazanym w ogłoszeniu, o którym mowa w art. 477 ust. 4 ustawy z dnia 11 września 
2019 r. – Prawo zamówień publicznych, zwanej dalej „ustawą”, w tym na adres tam wskazany. Za datę złożenia zgłoszenia 
uważa się datę jego wpływu na adres, o którym mowa w zdaniu pierwszym. Zgłoszenie uznaje się za złożone w terminie, 
jeżeli wpłynie ono na wskazany adres nie później niż w dniu, w którym upływa termin przyjmowania zgłoszeń. 

2. Zgłoszenie kandydata na członka Izby, zwane dalej „zgłoszeniem”, zawiera wniosek o dopuszczenie kandydata do 

postępowania kwalifikacyjnego, w którym: 

1) 

2) 

podaje się imię i nazwisko, drugie imię – o ile kandydat je posiada, datę i miejsce urodzenia, numer PESEL, imiona 
rodziców, serię i numer dowodu osobistego, adres miejsca zameldowania kandydata, adres jego miejsca zamieszkania 
oraz adres do korespondencji, jeżeli jest inny niż adres miejsca zamieszkania; 

opisuje się posiadane przez kandydata wykształcenie, doświadczenie oraz przebieg kariery zawodowej, odpowiadające 
warunkom, o których mowa w art. 474 ust. 2 pkt 2–4 ustawy. 

  
 
 
Dziennik Ustaw 

– 2 – 

Poz. 381 

3. Do  zgłoszenia  kandydat  na  członka  Izby,  zwany  dalej  „kandydatem”,  może  dołączyć  oświadczenie  o wyrażeniu 
zgody  na  doręczanie  mu  pism  w postępowaniu  kwalifikacyjnym  przy  użyciu  środków  komunikacji  elektronicznej  wraz 
z podaniem adresu poczty elektronicznej, pod jakim korespondencja będzie przez kandydata odbierana. Złożenie zgłoszenia 
bez oświadczenia, o którym mowa w zdaniu pierwszym, jest równoznaczne z brakiem zgody na doręczanie pism w postępo-
waniu kwalifikacyjnym przy użyciu środków komunikacji elektronicznej. 

4. Do  zgłoszenia,  w celu  potwierdzenia  spełniania  warunków,  o których  mowa  w art. 474 ust. 2 pkt 1–6  i 8–10  ustawy, 

kandydat dołącza: 

1) 

2) 

3) 

4) 

5) 

6) 

7) 

8) 

oświadczenie o posiadaniu polskiego obywatelstwa; 

kopię dyplomu potwierdzającego ukończenie studiów na kierunku prawo; 

kopię  uchwały  o uzyskaniu  pozytywnego  wyniku  z egzaminu  sędziowskiego,  prokuratorskiego,  notarialnego,  adwo-
kackiego lub radcowskiego lub kopię powołania na stanowisko sędziowskie, prokuratorskie lub stanowisko notariu-
sza,  lub  kopię  uchwały  właściwego  organu  samorządu  zawodowego  adwokatury  lub  samorządu  radców  prawnych 
o wpisie odpowiednio na listę adwokatów lub radców prawnych; 

oświadczenie  o posiadaniu  minimum  pięcioletniego  doświadczenia  zawodowego  w zakresie  zamówień  publicznych 
oraz prawa cywilnego, wraz z kopią dokumentów potwierdzających to doświadczenie; 

oświadczenie o posiadaniu pełnej zdolności do czynności prawnych; 

oświadczenie o korzystaniu z pełni praw publicznych; 

aktualną informację z Krajowego Rejestru Karnego, wystawioną nie wcześniej niż 30 dni przed dniem złożenia zgło-
szenia, potwierdzającą, że nie był skazany prawomocnym wyrokiem za przestępstwo umyślne lub umyślne przestęp-
stwo skarbowe; 

kopię poświadczenia bezpieczeństwa upoważniającego do dostępu do informacji niejawnych o klauzuli „poufne” lub 
wyższej  albo  oświadczenie  o wyrażeniu  zgody  na  przeprowadzenie  postępowania  sprawdzającego,  o którym  mowa 
w art. 22 ust. 1 pkt 1 lub 2 ustawy z dnia 5 sierpnia 2010 r. o ochronie informacji niejawnych (Dz. U. z 2019 r. poz. 742). 

§ 3. 1.  W przypadku  gdy  kandydat  nie  dołączył  do  zgłoszenia dokumentów, o których mowa  w § 2 ust. 4, lub dołą-
czone  dokumenty  zawierają  błędy,  lub  w zgłoszeniu  nie  podał  danych,  o których  mowa  w § 2 ust. 2,  lub  nie  zostało ono 
opatrzone własnoręcznym podpisem kandydata, w przypadku zgłoszenia składanego w formie pisemnej, lub kwalifikowa-
nym  podpisem  elektronicznym,  podpisem  osobistym  lub  podpisem  zaufanym,  w przypadku  zgłoszenia  składanego 
w postaci elektronicznej, kandydat wzywany jest do uzupełnienia braków formalnych, w tym uzupełnienia lub poprawienia 
dokumentów lub zgłoszenia, w terminie 7 dni od dnia doręczenia wezwania. 

2. Kandydat  składa  uzupełnione  lub  poprawione  dokumenty  lub  zgłoszenie  w miejscu,  o którym  mowa  w § 2 ust. 1 

zdanie pierwsze. Przepisy § 2 ust. 1 zdanie drugie i trzecie stosuje się odpowiednio. 

3. Niezwłocznie  po  upływie  terminu  przyjmowania  zgłoszeń  lub  upływie  terminu  uzupełnienia  braków  formalnych 
komisja kwalifikacyjna powołana do przeprowadzenia postępowania kwalifikacyjnego, zwana dalej „komisją”, podejmuje 
uchwałę o dopuszczeniu kandydata do udziału w postępowaniu kwalifikacyjnym. 

4. Jeżeli  kandydat  nie  uzupełnił  w terminie  braków  formalnych  lub  złożył  zgłoszenie  po  terminie,  o którym  mowa 

w art. 477 ust. 4 pkt 2 ustawy, komisja podejmuje uchwałę o odmowie dopuszczenia kandydata do udziału w tym postępowaniu. 

5. Przewodniczący komisji, za pośrednictwem operatora pocztowego, w rozumieniu ustawy z dnia 23 listopada 2012 r. – 
Prawo  pocztowe  (Dz. U.  z 2020 r.  poz. 1041  i 2320),  zwanego  dalej  „operatorem  pocztowym”,  przesyłką  poleconą,  za 
pośrednictwem posłańca albo przy użyciu środków komunikacji elektronicznej, o ile kandydat złożył oświadczenie, o którym 
mowa w § 2 ust. 3, pouczając o prawie do wniesienia odwołania, terminie na jego wniesienie, organie, do którego należy je 
wnieść, oraz adresie, na który odwołanie powinno być wniesione, zawiadamia: 

1) 

2) 

kandydatów  dopuszczonych  do  udziału  w postępowaniu  kwalifikacyjnym  –  o miejscu  i terminie  przeprowadzenia 
egzaminu pisemnego; 

pozostałych kandydatów – o odmowie dopuszczenia ich do udziału w postępowaniu kwalifikacyjnym, podając przy-
czyny odmowy dopuszczenia. 

6. W przypadku wniesienia przez kandydata odwołania od uchwały komisji kwalifikacyjnej o odmowie dopuszczenia 

do udziału w postępowaniu kwalifikacyjnym termin 7 dni na jego wniesienie jest liczony od dnia doręczenia uchwały. 

  
 
 
Dziennik Ustaw 

– 3 – 

Poz. 381 

7. Minister właściwy do spraw gospodarki: 

odrzuca odwołanie złożone po upływie terminu na wniesienie odwołania albo 

oddala odwołanie i utrzymuje w mocy uchwałę komisji, albo 

uwzględnia odwołanie, zmienia uchwałę komisji i dopuszcza kandydata do udziału w postępowaniu kwalifikacyjnym. 

1) 

2) 

3) 

8. Minister właściwy do spraw gospodarki niezwłocznie, nie później niż w terminie 7 dni od dnia wniesienia odwoła-
nia,  przekazuje  kandydatowi rozstrzygnięcie odwołania za pośrednictwem operatora pocztowego, przesyłką  poleconą, za 
pośrednictwem  posłańca  albo  przy  użyciu  środków  komunikacji  elektronicznej,  o ile  kandydat  złożył  oświadczenie, 
o którym mowa w § 2 ust. 3. 

Rozdział 3 

Tryb przeprowadzania postępowania kwalifikacyjnego oraz wnoszenie odwołania od wyniku tego postępowania 

§ 4. 1. Komisja przeprowadza egzamin pisemny w warunkach umożliwiających kandydatom samodzielną pracę, pod 

nadzorem przewodniczącego komisji oraz jej członków. 

2. Wszyscy zgłoszeni kandydaci przystępują do sprawdzianu wiedzy oraz sporządzenia pracy pisemnej w tym samym 

terminie i w tym samym miejscu. 

§ 5. 1. Egzamin pisemny składa się: 

1) 

2) 

ze  sprawdzianu  wiedzy,  mającego  na  celu  sprawdzenie  teoretycznej  wiedzy  z zakresu  zamówień  publicznych  oraz 
prawa cywilnego; 

z  pracy  pisemnej,  mającej  na  celu  sprawdzenie  praktycznej  wiedzy  z zakresu  zamówień  publicznych  oraz  prawa  
cywilnego. 

2. Przerwa między częściami egzaminu pisemnego nie może trwać dłużej niż 30 minut. 

3. Egzamin pisemny jest przeprowadzany w oparciu o zestaw materiałów do sprawdzianu wiedzy oraz zestaw mate-
riałów do pracy pisemnej. Zestawy wybiera komisja spośród propozycji czterech zestawów materiałów służących do prze-
prowadzenia  każdej  z części  egzaminu  pisemnego,  zawierających  pytania  testowe  do  sprawdzianu  wiedzy  i zadanie  do 
pracy pisemnej oraz arkusze odpowiedzi i klucze ich oceny, przygotowane z uwzględnieniem zakresu zagadnień do prze-
prowadzenia postępowania kwalifikacyjnego określonego w załączniku nr 1 do rozporządzenia. 

4. Wybrane zestawy są: 

1) 

2) 

3) 

powielane w liczbie odpowiadającej liczbie kandydatów dopuszczonych do udziału w postępowaniu kwalifikacyjnym 
i umieszczane  w zamkniętych  opakowaniach  opatrzonych  pieczęcią  Urzędu  Zamówień  Publicznych,  zwanego  dalej 
„Urzędem”; 

przechowywane i zabezpieczone w siedzibie Urzędu w sposób uniemożliwiający ich nieuprawnione ujawnienie; 

dostarczane w zamkniętych opakowaniach opatrzonych pieczęcią Urzędu na salę egzaminacyjną przez przewodniczą-
cego komisji i wskazanego przez niego członka komisji, najwcześniej na pół godziny przed rozpoczęciem egzaminu 
pisemnego. 

§ 6. 1. Sprawdzian wiedzy ma formę testu jednokrotnego wyboru i składa się z 50 pytań. Za każdą prawidłową odpo-
wiedź kandydat otrzymuje dwa punkty, a za brak odpowiedzi – zero punktów. Maksymalna liczba punktów możliwych do 
uzyskania ze sprawdzianu wiedzy wynosi 100. 

2. Sprawdzian wiedzy trwa 60 minut. 

§ 7. 1. Praca pisemna polega na dokonaniu przez kandydata oceny prawnej stanu faktycznego (kazusu) na podstawie 
zestawu  materiałów  do  pracy  pisemnej,  o którym  mowa  w § 5 ust. 3,  składającego się z dokumentów zakwalifikowanych 
przez komisję na potrzeby egzaminu pisemnego lub dokumentów opracowanych na potrzeby tego egzaminu. Każdy kan-
dydat ocenia ten sam stan faktyczny. 

2. Praca pisemna trwa 120 minut. 

3. Praca  pisemna  jest  oceniana  pod  względem  merytorycznym,  języka  i stylu  pracy,  zgodnie  z punktacją  określoną 

w załączniku nr 2 do rozporządzenia. Maksymalna liczba punktów możliwych do uzyskania z pracy pisemnej wynosi 100. 

  
 
 
Dziennik Ustaw 

– 4 – 

Poz. 381 

§ 8. 1. Przed rozpoczęciem sprawdzianu wiedzy: 

1) 

kandydat: 

a)  okazuje  dokument  zawierający  zdjęcie,  potwierdzający  jego  tożsamość,  i potwierdza  własnoręcznym  podpisem 

na liście obecności udział w sprawdzianie wiedzy, 

b) 

losuje kopertę z oznaczeniem „Sprawdzian wiedzy”, w której znajduje się karta z indywidualnym numerem kodu 
do oznaczenia arkusza odpowiedzi do sprawdzianu wiedzy, 

c)  przekazuje komisji do depozytu na czas trwania egzaminu pisemnego wyłączony telefon komórkowy i inne urzą-
dzenia służące do komunikacji elektronicznej, o ile takie posiada; telefon i inne złożone do depozytu urządzenia 
służące do komunikacji elektronicznej przechowuje się w zaklejonych i podpisanych kopertach lub opakowaniach; 

2) 

przewodniczący, wiceprzewodniczący lub sekretarz komisji informuje kandydatów o: 

a)  warunkach organizacyjnych i sposobie przeprowadzenia egzaminu pisemnego, 

b)  przepisach porządkowych obowiązujących w trakcie przeprowadzania egzaminu pisemnego, 

c)  zasadach dokonywania oceny udzielonych odpowiedzi oraz oceny pracy pisemnej, 

d)  sposobie zawiadomienia o wynikach egzaminu pisemnego, 

e)  sposobie zawiadomienia o terminie i miejscu przeprowadzenia rozmowy kwalifikacyjnej. 

2. Otwarcie  opatrzonego  pieczęcią  Urzędu  opakowania  z pytaniami  testowymi  następuje  w dniu  i o godzinie  rozpo-
częcia  sprawdzianu  wiedzy,  w obecności  osób  przystępujących  do  egzaminu  pisemnego.  Z czynności  tej  sporządza  się 
protokół podpisany przez członków komisji. 

3. Przed rozpoczęciem sprawdzianu wiedzy kandydat: 

1) 

otrzymuje opatrzone pieczęcią Urzędu: 

a)  arkusz z pytaniami testowymi, 

b)  arkusz odpowiedzi do sprawdzianu wiedzy; 

2)  wpisuje w prawym górnym rogu na pierwszej stronie arkusza odpowiedzi do sprawdzianu wiedzy indywidualny numer 

kodu znajdujący się na karcie w wylosowanej kopercie; 

3) 

zapisuje swoje imię i nazwisko na karcie z numerem kodu, umieszcza kartę z powrotem w kopercie, zakleja i oddaje 
komisji. 

§ 9. 1. Przed rozpoczęciem pracy pisemnej kandydat: 

1) 

2) 

okazuje  dokument  zawierający  zdjęcie,  potwierdzający  jego  tożsamość,  i potwierdza  własnoręcznym  podpisem  na 
liście obecności udział w pracy pisemnej; 

losuje kopertę z oznaczeniem „Praca pisemna”, w której znajduje się karta z indywidualnym numerem kodu do ozna-
czenia pracy pisemnej. 

2. Otwarcie opatrzonego pieczęcią Urzędu opakowania z zadaniem pracy pisemnej następuje w dniu i o godzinie roz-
poczęcia pracy pisemnej w obecności osób przystępujących do egzaminu pisemnego. Z czynności tej sporządza się proto-
kół podpisany przez wszystkich członków komisji obecnych podczas pracy pisemnej. 

3. Przed rozpoczęciem pracy pisemnej kandydat: 

1) 

otrzymuje opatrzone pieczęcią Urzędu: 

a)  nieoznakowaną zaklejoną kopertę z zadaniem pracy pisemnej, 

b)  spięte i ponumerowane czyste karty przeznaczone do sporządzenia pracy pisemnej; 

2)  wpisuje w prawym górnym rogu na pierwszej czystej karcie przeznaczonej do sporządzenia pracy pisemnej indywi-

dualny numer kodu znajdujący się na karcie w wylosowanej kopercie; 

3) 

zapisuje swoje imię i nazwisko na karcie z numerem kodu, umieszcza kartę z powrotem w kopercie, zakleja i oddaje 
komisji. 

§ 10. 1. Przewodniczący komisji, wiceprzewodniczący lub sekretarz tej komisji wyklucza bez ostrzeżenia z postępo-

wania kwalifikacyjnego kandydata, który podczas sprawdzianu wiedzy lub pracy pisemnej: 

1) 

2) 

korzysta z pomocy innej osoby; 

posługuje się niedozwolonymi materiałami; 

  
 
 
Dziennik Ustaw 

– 5 – 

Poz. 381 

3) 

4) 

korzysta z urządzeń służących do przekazu lub odbioru informacji; 

porozumiewa się lub pomaga pozostałym kandydatom; 

5)  w inny sposób zakłóca przebieg postępowania kwalifikacyjnego. 

2. Wykluczenie zostaje odnotowane w protokole z przebiegu egzaminu pisemnego oraz na arkuszu odpowiedzi lub na 

pracy pisemnej. 

3. W trakcie egzaminu pisemnego kandydat może opuścić salę egzaminacyjną po uzyskaniu zgody przewodniczącego 
komisji  kwalifikacyjnej,  pod  nadzorem  członka  komisji  wskazanego  przez  przewodniczącego.  Przed  opuszczeniem  sali 
kandydat  przekazuje  arkusz  odpowiedzi  do  sprawdzianu  wiedzy  lub  pracę  pisemną przewodniczącemu komisji.  Członek 
komisji odnotowuje na egzemplarzu arkusza odpowiedzi lub pracy pisemnej kandydata godzinę wyjścia i powrotu na salę. 

4. Opuszczenie  przez  kandydata  sali  egzaminacyjnej  bez  zgody  przewodniczącego  komisji  stanowi  zakończenie 
udziału w postępowaniu kwalifikacyjnym. Przed opuszczeniem sali egzaminacyjnej kandydat zwraca arkusz odpowiedzi do 
sprawdzianu wiedzy lub pracę pisemną. 

5. Po  upływie  czasu  wskazanego odpowiednio w § 6 ust. 2 i § 7 ust. 2 członkowie komisji zbierają arkusze z pytaniami 
testowymi i arkusze odpowiedzi do sprawdzianu wiedzy albo zadanie pracy pisemnej i pracę pisemną. W momencie oddania 
kandydat  otrzymuje  pokwitowanie  odbioru  odpowiednio  arkusza  z pytaniami  testowymi  i arkusza  odpowiedzi  do  spraw-
dzianu wiedzy albo zadania pracy pisemnej i pracy pisemnej. 

6. Z czynności  odbioru  odpowiednio  arkusza  z pytaniami  testowymi  i arkusza  odpowiedzi  do  sprawdzianu  wiedzy  
albo zadania pracy pisemnej i pracy pisemnej sporządza się protokół, który zawiera w szczególności godzinę odbioru dla 
danego numeru kodu i podpis odbierającego członka komisji kwalifikacyjnej. 

7. Członkowie  komisji  obecni  w sali  egzaminacyjnej  sporządzają  protokół  z przebiegu  egzaminu  pisemnego,  który 
zawiera  imiona  i nazwiska  członków  komisji,  oznaczenie  godziny  rozpoczęcia  i zakończenia  sprawdzianu  wiedzy  oraz 
pracy  pisemnej,  liczbę  osób  uczestniczących  w każdej  części  egzaminu  pisemnego,  informację  o wykluczeniu  kandydata 
albo opuszczeniu przez niego sali egzaminacyjnej bez zgody przewodniczącego  – oznaczonego indywidualnym numerem 
kodu  znajdującego  się  na  karcie  w wylosowanej  kopercie  do  oznaczenia  odpowiednio  sprawdzianu  wiedzy  albo  pracy 
pisemnej, o ile wykluczenie albo opuszczenie miało miejsce, a także uwagi dotyczące przebiegu egzaminu, oraz podpisy 
członków komisji egzaminacyjnej. 

§ 11. 1. Komisja uwzględnia tylko odpowiedzi udzielone na arkuszu odpowiedzi do sprawdzianu wiedzy oraz kartach 

przeznaczonych do sporządzenia pracy pisemnej, opatrzonych pieczęcią Urzędu. 

2. Arkusze  odpowiedzi  do  sprawdzianu  wiedzy  i prace  pisemne  kandydatów  oznaczone  indywidualnymi  kodami  są 

rozkodowywane po sprawdzeniu wszystkich prac. 

3. Do rozkodowania arkuszy odpowiedzi do sprawdzianu wiedzy i prac pisemnych przewodniczący komisji wyznacza 
co najmniej dwóch członków tej komisji, którzy nie sprawdzali odpowiedzi albo prac pisemnych. Z rozkodowania sporzą-
dza się protokół, który podpisują członkowie komisji dokonujący rozkodowania. 

§ 12. Po zakończeniu egzaminu pisemnego komisja ustala wyniki. Przewodniczący komisji zawiadamia kandydatów 
za  pośrednictwem  operatora  pocztowego,  przesyłką  poleconą,  za  pośrednictwem  posłańca  albo  przy  użyciu  środków  
komunikacji elektronicznej, o ile kandydat złożył oświadczenie, o którym mowa w § 2 ust. 3, o: 

1)  wynikach egzaminu pisemnego z podziałem na sprawdzian wiedzy i pracę pisemną, 

2) 

terminie i miejscu przeprowadzenia rozmowy kwalifikacyjnej albo o odmowie dopuszczenia do rozmowy kwalifika-
cyjnej 

– pouczając  o prawie  do  wniesienia  odwołania  od  wyników,  terminie  na  jego  wniesienie,  organie,  do  którego  należy  je 
wnieść, oraz adresie, na który odwołanie powinno być wniesione. 

§ 13. 1. Do rozmowy kwalifikacyjnej dopuszcza się kandydatów, którzy uzyskali nie mniej niż 50 punktów ze spraw-

dzianu wiedzy oraz nie mniej niż 50 punktów z pracy pisemnej. 

2. Jeżeli liczba kandydatów, którzy uzyskali minimalną liczbę punktów ze sprawdzianu wiedzy oraz pracy pisemnej, 
jest większa niż dwukrotność limitu osób określonego w ogłoszeniu o postępowaniu kwalifikacyjnym, do rozmowy kwali-
fikacyjnej  zaprasza  się  kandydatów,  którzy  otrzymali  kolejno  największą  liczbę punktów w ramach dwukrotności limitu. 
Limit  może  zostać  przekroczony,  jeżeli  dwóch  lub  więcej  kandydatów  uzyska  taką  samą  liczbę  punktów  umożliwiającą 
zaproszenie do rozmowy kwalifikacyjnej w ramach dwukrotności limitu. 

§ 14. 1. Podczas rozmowy kwalifikacyjnej sprawdza się cechy osobowe kandydata, kompetencje i predyspozycje do 
wykonywania obowiązków członka Izby, biorąc pod uwagę kryteria oceny określone w załączniku nr 3 do rozporządzenia. 

  
 
 
Dziennik Ustaw 

– 6 – 

Poz. 381 

2. Rozmowa kwalifikacyjna z kandydatem trwa nie dłużej niż 20 minut. 

3. Rozmowę  z kandydatem  przeprowadza  komisja  w składzie  co  najmniej  trzech  jej  członków.  Komisja  nie  może 

w tym samym czasie przeprowadzać rozmowy kwalifikacyjnej z więcej niż jednym kandydatem. 

4. Ocena jest sporządzana przez każdego członka komisji biorącego udział w rozmowie kwalifikacyjnej. Ocenę kan-
dydata w danym kryterium stanowi średnia arytmetyczna ocen członków komisji. Maksymalna liczba punktów możliwych 
do uzyskania podczas rozmowy kwalifikacyjnej wynosi 30. O uzyskanej liczbie punktów podczas rozmowy kwalifikacyj-
nej  przewodniczący  komisji  zawiadamia  kandydatów  za  pośrednictwem  operatora  pocztowego,  przesyłką  poleconą,  za 
pośrednictwem  posłańca  albo  przy  użyciu  środków  komunikacji  elektronicznej,  o ile  kandydat  złożył  oświadczenie, 
o którym mowa w § 2 ust. 3. 

5. Po zakończeniu wszystkich rozmów kwalifikacyjnych przewodniczący komisji zawiadamia kandydatów, którzy zo-
stali  dopuszczeni  do  rozmowy  kwalifikacyjnej,  o zakończeniu  rozmów  oraz  o ostatecznych  wynikach  za  pośrednictwem 
operatora  pocztowego,  przesyłką  poleconą,  za  pośrednictwem  posłańca  albo  przy  użyciu  środków  komunikacji  elektro-
nicznej,  o ile  kandydat  złożył  oświadczenie,  o którym  mowa  w § 2 ust. 3,  pouczając  o prawie  do  wniesienia  odwołania, 
terminie na jego wniesienie, organie, do którego należy je wnieść, oraz adresie, na który odwołanie powinno być wniesione. 

§ 15. 1. W przypadku wniesienia przez kandydata odwołania od uchwały komisji dotyczącej wyników: 

1) 

2) 

egzaminu pisemnego, 

rozmowy kwalifikacyjnej 

– termin  7 dni  na  wniesienie odwołania  jest liczony od dnia otrzymania zawiadomienia o wyniku, o którym mowa odpo-
wiednio w § 12 albo § 14 ust. 5. 

2. Odwołanie wnosi się na adres wskazany w zawiadomieniu, o którym mowa odpowiednio w § 12 albo § 14 ust. 5. 
Odwołanie może być wniesione osobiście, za pośrednictwem operatora pocztowego, przesyłką poleconą albo za pośrednic-
twem posłańca, a także przy użyciu środków komunikacji elektronicznej, w tym na elektroniczną skrzynkę podawczą urzę-
du  obsługującego  ministra  właściwego  do  spraw  gospodarki  lub  na  adres  poczty  elektronicznej,  jeżeli  został  wskazany 
w zawiadomieniu,  o którym  mowa  odpowiednio  w § 12  albo  § 14 ust. 5.  Odwołanie  wnoszone  w postaci  elektronicznej 
wymaga  opatrzenia,  przez  wnoszącego,  kwalifikowanym  podpisem  elektronicznym,  podpisem  osobistym  albo  podpisem 
zaufanym. 

3. Za  datę  wniesienia  odwołania  uznaje  się  datę  wpływu  odwołania  na  adres  wskazany  w zawiadomieniu,  o którym 
mowa odpowiednio w § 12 albo § 14 ust. 5, albo datę złożenia odwołania w placówce pocztowej operatora wyznaczonego, 
w rozumieniu  ustawy  z dnia  23 listopada  2012 r.  –  Prawo  pocztowe,  jeżeli  odwołanie zostało złożone za pośrednictwem 
tego operatora. Jeżeli odwołanie jest wnoszone w postaci elektronicznej, za datę jego wniesienia uznaje się datę wpływu 
odwołania  na  elektroniczną  skrzynkę  podawczą  urzędu  obsługującego  ministra  właściwego  do  spraw  gospodarki  lub  na 
adres poczty elektronicznej, wskazany w zawiadomieniu, o którym mowa odpowiednio w § 12 albo § 14 ust. 5. 

4. W terminie na wniesienie odwołania kandydat ma prawo wglądu do: 

dotyczącej jego osoby uchwały komisji wraz z jej uzasadnieniem; 

dokumentów zawierających jego indywidualne wyniki; 

zawiadomień dotyczących jego osoby. 

1) 

2) 

3) 

5. Kandydat ma prawo sporządzenia notatek oraz fotokopii dokumentów zawierających jego indywidualne wyniki. 

6. Minister właściwy do spraw gospodarki, po zasięgnięciu opinii Prezesa Urzędu Zamówień Publicznych, rozpatruje 

odwołanie w terminie 10 dni od dnia upływu terminu na wniesienie odwołania. 

1) 

2) 

3) 

7. Minister właściwy do spraw gospodarki: 

odrzuca odwołanie złożone po upływie terminu na wniesienie odwołania albo 

oddala odwołanie i utrzymuje w mocy uchwałę komisji, albo 

uwzględnia odwołanie, uchyla czynności komisji podjęte w zakresie dotyczącym kandydata, którego odwołanie zosta-
ło uwzględnione, i nakazuje ich powtórzenie. 

8. Minister  właściwy  do  spraw  gospodarki  niezwłocznie  przekazuje  kandydatowi  rozstrzygnięcie  odwołania  za  po-
średnictwem operatora pocztowego, przesyłką poleconą, za pośrednictwem posłańca albo przy użyciu środków komunika-
cji elektronicznej, o ile kandydat złożył oświadczenie, o którym mowa w § 2 ust. 3. 

9. Na czynność komisji wykonaną w wyniku rozpatrzenia odwołania nie przysługuje odwołanie. 

  
 
 
Dziennik Ustaw 

– 7 – 

Poz. 381 

§ 16. 1. Po zakończeniu postępowania kwalifikacyjnego i rozpatrzeniu odwołań, o ile zostały wniesione: 

1) 

2) 

komisja  ustala,  w drodze  uchwały,  ocenę  końcową  z postępowania  kwalifikacyjnego,  którą  stanowi  suma  punktów 
uzyskanych przez kandydata z egzaminu pisemnego i rozmowy kwalifikacyjnej; 

przewodniczący komisji zawiadamia pisemnie, za potwierdzeniem odbioru, kandydatów, którzy zostali dopuszczeni 
do postępowania kwalifikacyjnego, o zakończeniu postępowania kwalifikacyjnego, z tym że kandydatów, którzy zo-
stali dopuszczeni do rozmowy kwalifikacyjnej, zawiadamia także o łącznej liczbie punktów uzyskanej przez każdego 
z nich  w postępowaniu  kwalifikacyjnym  wraz  z informacją,  czy  uzyskana  liczba  punktów  uprawnia  do  powołania 
kandydata na członka Izby. 

2. Po ustaleniu przez komisję wyników postępowania kwalifikacyjnego w Biuletynie Informacji Publicznej, na stronie 
podmiotowej  Kancelarii  Prezesa  Rady  Ministrów  oraz  stronie  podmiotowej  Urzędu  niezwłocznie  zamieszcza  się  wyniki 
wraz z łączną liczbą punktów uzyskaną przez tych kandydatów, którzy uzyskali liczbę punktów uprawniającą do powołania 
na  członka  Izby,  oraz  podaniem  imion  i nazwisk  tych  kandydatów,  a także  imion  ich  rodziców.  Wyniki  są  udostępniane 
przez 6 miesięcy od dnia ich zamieszczenia. 

§ 17. 1.  Jeżeli  dwóch  lub  więcej  kandydatów  uzyskało  w postępowaniu  kwalifikacyjnym  taką  samą  liczbę  punktów 
umożliwiającą powołanie na członka Izby, minister właściwy do spraw gospodarki powołuje tego kandydata lub tych kan-
dydatów, którzy uzyskali wyższą  liczbę  punktów z pracy pisemnej, a jeżeli liczba punktów uzyskanych z pracy pisemnej 
jest taka sama – kandydatów, którzy uzyskali wyższą liczbę punktów z rozmowy kwalifikacyjnej. 

2. Jeżeli  po  przeprowadzeniu  czynności  określonych  w ust. 1 nie  jest możliwe wyłonienie kandydatów bez przekro-
czenia limitu osób określonego w ogłoszeniu o postępowaniu kwalifikacyjnym, żaden z kandydatów, którzy uzyskali taką 
samą liczbę punktów, nie jest powoływany. 

Rozdział 4 

Powołanie komisji kwalifikacyjnej, organizacja jej pracy oraz wymagania wobec członków komisji 

§ 18. 1. Minister właściwy do spraw gospodarki powołuje komisję do przeprowadzenia postępowania kwalifikacyjne-
go  niezwłocznie  po  upływie  terminu  przyjmowania  zgłoszeń  i podaje  do  publicznej  wiadomości  imiona  i nazwiska  jej 
członków. Informację o składzie komisji zamieszcza się w Biuletynie Informacji Publicznej, na stronie podmiotowej Urzę-
du, na stronie podmiotowej urzędu obsługującego ministra właściwego do spraw gospodarki oraz na stronie podmiotowej 
Kancelarii Prezesa Rady Ministrów. 

2. Przewodniczącego  komisji  wyznacza  minister  właściwy  do  spraw  gospodarki,  spośród  członków  komisji.  Człon-
kowie  komisji  wybierają  ze  swojego  grona  wiceprzewodniczącego  komisji,  który  wykonuje  zadania  przewodniczącego 
komisji w przypadku jego nieobecności, oraz sekretarza komisji. 

1) 

2) 

3) 

3. Członkiem komisji nie może być osoba: 

kandydująca na członka Izby; 

pozostająca w związku małżeńskim albo we wspólnym pożyciu, w stosunku pokrewieństwa lub powinowactwa w linii 
prostej, pokrewieństwa lub powinowactwa w linii bocznej do drugiego stopnia albo związana z tytułu przysposobie-
nia, opieki lub kurateli z kandydatem; 

pozostająca z kandydatem w takim stosunku prawnym lub faktycznym, że może to budzić uzasadnione wątpliwości co 
do jej bezstronności. 

4. Członek komisji, niezwłocznie po zapoznaniu się ze złożonymi zgłoszeniami, składa oświadczenie o braku lub ist-
nieniu okoliczności, o których mowa w ust. 3. W przypadku istnienia okoliczności, o których mowa w ust. 3, minister właś- 
ciwy  do  spraw  gospodarki  dokonuje  zmiany  w składzie  komisji  i uznaje  za  nieważne  dotychczasowe  czynności  komisji, 
jeżeli osoba, o której mowa w ust. 3, brała w nich udział. Komisja w składzie zmienionym powtarza unieważnione czynności, 
z wyjątkiem czynności faktycznych, które nie mają wpływu na wynik postępowania kwalifikacyjnego. 

5. Obsługę organizacyjno-techniczną postępowania kwalifikacyjnego zapewnia Urząd Zamówień Publicznych. 

§ 19. 1. Komisja obraduje na posiedzeniach. Pierwsze posiedzenie komisji zwołuje minister właściwy do spraw gos- 

podarki nie później niż w terminie 7 dni od dnia powołania komisji. 

2. Komisja podejmuje uchwały jednomyślnie, przy obecności co najmniej 2/3 składu komisji. W przypadku nieosiąg- 
nięcia jednomyślności przewodniczący komisji zarządza przeprowadzenie imiennego głosowania za pomocą kart do gło-
sowania,  przy  obecności  co  najmniej  2/3 składu  komisji.  W przypadku  określonym  w zdaniu  drugim,  uchwały  są  podej-
mowane większością głosów, a w razie równej liczby głosów rozstrzyga głos przewodniczącego komisji. 

3. Z posiedzenia komisji jest sporządzany protokół, który podpisują członkowie komisji obecni na posiedzeniu. 

  
 
 
Dziennik Ustaw 

– 8 – 

Rozdział 5 

Uzupełniające postępowanie kwalifikacyjne 

Poz. 381 

§ 20. Do przeprowadzenia uzupełniającego postępowania kwalifikacyjnego stosuje się odpowiednio przepisy § 2–19. 

§ 21. Rozporządzenie wchodzi w życie po upływie 14 dni od dnia ogłoszenia.1) 

Rozdział 6 

Przepis końcowy 

Prezes Rady Ministrów: M. Morawiecki 

1)  Niniejsze rozporządzenie było poprzedzone rozporządzeniem Prezesa Rady Ministrów z dnia 2 lipca 2007 r. w sprawie trybu prze-
prowadzania  postępowania  kwalifikacyjnego  na  członków  Krajowej  Izby  Odwoławczej,  sposobu  powoływania  komisji  kwalifika-
cyjnej, a także szczegółowego zakresu postępowania kwalifikacyjnego (Dz. U. z 2018 r. poz. 1126), które traci moc z dniem wejścia 
w życie  niniejszego  rozporządzenia  na  podstawie  art. 97  ustawy  z dnia  11 września  2019 r.  –  Przepisy  wprowadzające  ustawę  – 
Prawo zamówień publicznych (Dz. U. poz. 2020 oraz z 2020 r. poz. 1086 i 2275). 

  
 
 
 
                                                           
Dziennik Ustaw 

– 9 – 

Poz. 381 

Załączniki  do  rozporządzenia  Prezesa  Rady  Ministrów 
z dnia 22 lutego 2021 r. (poz. 381) 

Załącznik nr 1 

ZAKRES ZAGADNIEŃ DO PRZEPROWADZENIA POSTĘPOWANIA KWALIFIKACYJNEGO 

Zakres zagadnień do przeprowadzenia postępowania kwalifikacyjnego obejmuje: 

1) 

przepisy ustawy z dnia 11 września 2019 r. – Prawo zamówień publicznych (Dz. U. poz. 2019 oraz z 2020 r. poz. 288, 
1492, 1517, 2275 i 2320) oraz ustawy z dnia 21 października 2016 r. o umowie koncesji na roboty budowlane lub 
usługi (Dz. U.  z 2019 r.  poz.  1528, 1655 i 2020 oraz z 2020 r. poz. 2275), przepisy aktów wykonawczych do tych 
ustaw, orzecznictwo Sądu Najwyższego, sądów okręgowych i Trybunału Konstytucyjnego dotyczące zamówień pub- 
licznych i koncesji na roboty budowlane lub usługi oraz główne kierunki orzecznicze Krajowej Izby Odwoławczej;  

2) 

przepisy prawa Unii Europejskiej dotyczące udzielania zamówień publicznych i koncesji, w tym:  

a)  zasady Traktatu o funkcjonowaniu Unii Europejskiej w zakresie dotyczącym zamówień publicznych i koncesji, 

b)  dyrektywę Parlamentu Europejskiego i Rady 2014/24/UE z dnia 26 lutego 2014 r. w sprawie zamówień publicz-

nych, uchylającą dyrektywę 2004/18/WE (Dz. Urz. UE L 94 z 28.03.2014, str. 65, z późn. zm.),  

c)  dyrektywę Parlamentu Europejskiego i Rady 2014/25/UE z dnia 26 lutego 2014 r. w sprawie udzielania zamó-
wień  przez  podmioty  działające  w  sektorach  gospodarki  wodnej,  energetyki,  transportu  i  usług  pocztowych, 
uchylającą dyrektywę 2004/17/WE (Dz. Urz. UE L 94 z 28.03.2014, str. 243, z późn. zm.),  

d)  dyrektywę Parlamentu Europejskiego i Rady 2014/23/UE z dnia 26 lutego 2014 r. w sprawie udzielania koncesji 

(Dz. Urz. UE L 94 z 28.03.2014, str. 1, z późn. zm.),  

e)  dyrektywę Parlamentu Europejskiego i Rady 2009/81/WE z dnia 13 lipca 2009 r. w sprawie koordynacji proce-
dur udzielania niektórych zamówień na roboty budowlane, dostawy i usługi przez instytucje lub podmioty zama-
wiające w dziedzinach obronności i bezpieczeństwa i zmieniającą dyrektywy 2004/17/WE i 2004/18/WE (Dz. Urz. 
UE L 216 z 20.08.2009, str. 76, z późn. zm.),  

f)  dyrektywę Parlamentu Europejskiego i Rady 2007/66/WE z dnia 11 grudnia 2007 r. zmieniającą dyrektywy Rady 
89/665/EWG  i  92/13/EWG  w  zakresie  poprawy  skuteczności  procedur  odwoławczych  w  dziedzinie  udzielania 
zamówień publicznych (Dz. Urz. UE L 335 z 20.12.2007, str. 31),  

g)  dyrektywę Rady 92/13/EWG z dnia 25 lutego 1992 r. koordynującą przepisy ustawowe, wykonawcze i admini-
stracyjne odnoszące się do stosowania przepisów wspólnotowych w procedurach zamówień publicznych podmio-
tów  działających  w sektorach  gospodarki  wodnej,  energetyki,  transportu  i  telekomunikacji  (Dz.  Urz.  WE  L  76 
z 23.03.1992, str. 14; Dz. Urz. UE Polskie wydanie specjalne, rozdz. 6, t. 1, str. 315),  

h)  dyrektywę Rady z dnia 21 grudnia 1989 r. w sprawie koordynacji przepisów ustawowych, wykonawczych i ad-
ministracyjnych  odnoszących  się  do  stosowania  procedur  odwoławczych  w  zakresie  udzielania  zamówień  pub- 
licznych na dostawy i roboty budowlane (Dz. Urz. WE L 395 z 30.12.1989, str. 33; Dz. Urz. UE Polskie wydanie 
specjalne, rozdz. 6, t. 1, str. 246),  

i) 

zasady  stosowania  Wspólnego  Słownika  Zamówień  określonego  w rozporządzeniu  (WE)  nr  2195/2002  Par- 
lamentu  Europejskiego  i  Rady  z  dnia  5  listopada  2002  r.  w sprawie  Wspólnego  Słownika  Zamówień  (CPV) 
(Dz. Urz.  WE  L  340  z 16.12.2002,  str.  1,  z  późn.  zm.;  Dz.  Urz.  UE  Polskie  wydanie  specjalne,  rozdz.  6,  t.  5, 
str. 3, z późn. zm.),  

j)  orzecznictwo  Trybunału  Sprawiedliwości  Unii  Europejskiej  dotyczące  udzielania  zamówień  publicznych  oraz 

bezpośredniego stosowania prawa Unii Europejskiej w państwach członkowskich Unii Europejskiej;  

3) 

4) 

5) 

6) 

przepisy ustawy z dnia 23 kwietnia 1964 r. – Kodeks cywilny (Dz. U. z 2020 r. poz. 1740 i 2320);  

przepisy  ustawy  z  dnia  17  listopada  1964  r.  –  Kodeks  postępowania  cywilnego  (Dz.  U.  z 2020  r.  poz.  1575,  1578 
i 2320 oraz z 2021 r. poz. 11), z wyłączeniem przepisów dotyczących postępowania zabezpieczającego i postępowa-
nia egzekucyjnego;  

przepisy ustawy z dnia 7 lipca 1994 r. – Prawo budowlane (Dz. U. z 2020 r. poz. 1333, 2127 i 2320 oraz z 2021 r. 
poz. 11, 234 i 282);  

przepisy ustawy z dnia 6 czerwca 1997 r. – Kodeks karny (Dz. U. z 2020 r. poz. 1444 i 1517) dotyczące przestępstw 
przeciwko obrotowi gospodarczemu;  

  
 
 
 
Dziennik Ustaw 

– 10 – 

Poz. 381 

7)  wybrane zagadnienia z dziedziny finansów publicznych:  

a) 

formy organizacyjno-prawne jednostek sektora finansów publicznych,  

b)  zasady gospodarki finansowej jednostek sektora finansów publicznych,  

c)  zasady i zakres odpowiedzialności za naruszenie dyscypliny finansów publicznych;  

8) 

przepisy  ustawy  z  dnia  14  czerwca  1960  r.  –  Kodeks  postępowania  administracyjnego  (Dz.  U.  z  2020  r. poz.  256, 
695, 1298 i 2320 oraz z 2021 r. poz. 54 i 187);  

9) 

zasady organizacji i funkcjonowania administracji rządowej oraz samorządu terytorialnego;  

10)  przepisy ustawy z dnia 15 września 2000 r. – Kodeks spółek handlowych (Dz. U. z 2020 r. poz. 1526 i 2320);  

11)  przepisy związane ze zwalczaniem nieuczciwej konkurencji;  

12)  przepisy o partnerstwie publiczno-prywatnym;  

13)  przepisy dotyczące zakresu przedmiotowego i podmiotowego prawa autorskiego, treści i ochrony prawa autorskiego;  

14)  przepisy o ochronie informacji niejawnych.  

  
 
 
 
 
 
Dziennik Ustaw 

– 11 – 

Poz. 381 

Załącznik nr 2 

ELEMENTY OCENY PRACY PISEMNEJ I PRZYPISANA IM PUNKTACJA 

Kryterium 

Liczba punktów 

Elementy oceny 

Wartość merytoryczna 
pracy 

0–90 pkt 

1)  prawidłowość konstrukcji orzeczenia (obligatoryjne elementy sentencji 

orzeczenia i uzasadnienia); 

2)  prawidłowość zastosowanych przepisów prawa i ich wykładni; 

3)  prawidłowość  dokonania  ustaleń  faktycznych,  oceny  dowodów  i  po-
prawność zaproponowanego przez zdającego sposobu rozstrzygnięcia; 

4)  sposób formułowania wypowiedzi. 

Język i styl pracy 

0–10 pkt 

1)  umiejętność posługiwania się językiem prawniczym; 

2)  poprawność użytego słownictwa; 

3)  umiejętność  jasnego  i  zwięzłego  wypowiadania  się  (zrozumiały  dla 

stron tok wywodu); 

4)  sposób formułowania wypowiedzi. 

  
 
 
 
 
 
Dziennik Ustaw 

– 12 – 

Poz. 381 

Załącznik nr 3  

KRYTERIA OCENY STOSOWANE PODCZAS ROZMOWY KWALIFIKACYJNEJ 

Kryterium 

Liczba punktów 

Elementy oceny 

Zdolności analityczne 

0–10 pkt 

1)  udzielanie poprawnej merytorycznie i wyczerpującej odpowiedzi; 

2)  umiejętność analizy i syntezy informacji; 

3)  umiejętność  zastosowania  posiadanej  wiedzy  w konkretnych  stanach 
faktycznych  i  prawnych  (znajomość  przepisów  związanych  z  obsza-
rem prawa zamówień publicznych, znajomość orzecznictwa krajowe-
go i unijnego); 

4)  umiejętność łączenia wiedzy z różnych dziedzin; 

5)  umiejętność podejmowania samodzielnych decyzji. 

Odporność na stres 
i asertywność 

0–10 pkt 

1)  umiejętność kontrolowania emocji i wytrzymywania obciążeń psychicz-

nych oraz adekwatnego reagowania na istniejącą sytuację; 

2)  umiejętność  reagowania  na  sytuację  w  sposób  zgodny  z  normami 

społecznymi i ustalonymi procedurami; 

3)  umiejętność jasnego wyrażania swojego stanowiska, w tym: 

a) 

b) 

nieuleganie wpływom i niepoddawanie się naciskom, 

zdolność  do  odróżniania  opinii  od  faktów,  argumentowania  na 
poziomie faktów, 

c) 

umiejętność ujawniania rozbieżności opinii. 

Komunikatywność 

0–5 pkt 

1)  spójność wypowiedzi, logiczny tok wywodu; 

2)  umiejętność jasnego i precyzyjnego wyrażania się oraz precyzowania 

myśli; 

3)  umiejętność słuchania i porozumiewania się z innymi. 

Styl wypowiedzi 

0–5 pkt 

1)  poprawność użytego słownictwa; 

2)  sposób formułowania wypowiedzi (brak nieuzasadnionego powtarzania); 

3)  umiejętność posługiwania się językiem prawniczym. 

  
 
 
 
DZIENNIK USTAW 
RZECZYPOSPOLITEJ POLSKIEJ 

Warszawa, dnia 23 marca 2021 r. 

Poz. 525 

RO ZPO RZĄDZENIE  

PREZESA RADY M INISTR Ó W 

z dnia 18 marca 2021 r. 

w sprawie wielokrotności kwoty bazowej stanowiącej podstawę ustalenia wynagrodzenia zasadniczego Prezesa, 
wiceprezesa oraz pozostałych członków Krajowej Izby Odwoławczej 

Na  podstawie  art. 484 ust. 5  ustawy  z dnia  11 września  2019 r.  –  Prawo  zamówień  publicznych  (Dz. U.  poz. 2019, 

z późn. zm.1)) zarządza się, co następuje: 

§ 1. Określa  się  następujące  wielokrotności  kwoty  bazowej  stanowiącej  podstawę  ustalenia  wynagrodzenia zasadni-

czego Prezesa Krajowej Izby Odwoławczej, zwanej dalej „Izbą”, wiceprezesa oraz pozostałych członków Izby: 

1)  Prezes Izby – 6,2; 

2)  wiceprezes Izby – 5,8; 

3) 

członek Izby inny niż wymieniony w pkt 1 i 2 – 5,6. 

§ 2. Rozporządzenie wchodzi w życie po upływie 14 dni od dnia ogłoszenia.2) 

Prezes Rady Ministrów: M. Morawiecki 

1)  Zmiany wymienionej ustawy zostały ogłoszone w Dz. U. z 2020 r. poz. 288, 1492, 1517, 2275 i 2320 oraz z 2021 r. poz. 464. 
2)  Niniejsze rozporządzenie było poprzedzone rozporządzeniem Prezesa Rady Ministrów z dnia 24 kwietnia 2018 r. w sprawie wielo-
krotności kwoty bazowej stanowiącej podstawę ustalenia wynagrodzenia zasadniczego Prezesa, wiceprezesa oraz pozostałych członków 
Krajowej Izby Odwoławczej (Dz. U. poz. 779), które traci moc z dniem wejścia w życie niniejszego rozporządzenia na podstawie 
art. 97 ustawy z dnia 11 września 2019 r. – Przepisy wprowadzające ustawę – Prawo zamówień publicznych (Dz. U. poz. 2020 oraz 
z 2020 r. poz. 1086 i 2275). 

  
 
 
                                                           
R O Z P O R Z Ą D Z E N I E    

M I N I S T R A   R O Z W O J U   I   T E C H N O L O G I I 1) 

z dnia 23 listopada 2021 r. 

w sprawie metody kalkulacji kosztów cyklu życia budynków oraz sposobu 

Warszawa, dnia 10 grudnia 2021 r.

przedstawiania informacji o tych kosztach 

Poz. 2276

Na  podstawie  art.  245  ust.  7  ustawy  z  dnia  11  września  2019  r.  –  Prawo  zamówień 

publicznych (Dz. U. z 2021 r. poz. 1129, 1598 i 2054) zarządza się, co następuje: 

RozpoRządzenie
MinistRa Rozwoju i technologii 1)

§  1.  Kalkulację  kosztów  cyklu  życia  budynku  oblicza  się  jako  sumę  kosztów  nabycia, 

użytkowania oraz utrzymania budynku, obliczoną według wzoru: 

z dnia 23 listopada 2021 r.

w sprawie metody kalkulacji kosztów cyklu życia budynków oraz sposobu przedstawiania informacji o tych kosztach

Cg = Cn + Cuz + Cut 

Na  podstawie  art.  245  ust.  7  ustawy  z  dnia  11  września  2019  r.  –  Prawo  zamówień  publicznych  (Dz.  U.  z  2021  r. 
gdzie: 

poz. 1129, 1598, 2054 i 2269) zarządza się, co następuje:

Cg  –  koszty  cyklu  życia  budynku  w  30-letnim  okresie  życia  budynku,  zwanym  dalej 
§ 1. Kalkulację kosztów cyklu życia budynku oblicza się jako sumę kosztów nabycia, użytkowania oraz utrzymania 

budynku, obliczoną według wzoru:

„okresem obliczeniowym”, 

Cn  – koszty nabycia, 

Cg = Cn + Cuz + Cut

Cuz  – koszty użytkowania,  

gdzie:

Cut  – koszty utrzymania.  

§ 2. Zamawiający określa koszt nabycia na podstawie ceny oferty. 

Cg  –  koszty cyklu życia budynku w 30-letnim okresie życia budynku, zwanym dalej „okresem obliczeniowym”,
Cn  –  koszty nabycia,
Cuz  –  koszty użytkowania,
Cut  –  koszty utrzymania.

§ 3. 1. Koszty użytkowania związane z przewidywanym zużyciem energii końcowej lub 

nośników energii oraz wody z uwzględnieniem odprowadzania ścieków oblicza się jako sumę 
§ 2. Zamawiający określa koszt nabycia na podstawie ceny oferty.
iloczynów ilości zużywanej w ciągu roku energii końcowej lub nośników energii oraz wody 

§ 3. 1. Koszty użytkowania związane z przewidywanym zużyciem energii końcowej lub nośników energii oraz wody 
z uwzględnieniem odprowadzania ścieków, cen jednostkowych energii końcowej lub nośnika 
z uwzględnieniem odprowadzania ścieków oblicza się jako sumę iloczynów ilości zużywanej w ciągu roku energii końco-
energii  oraz  wody  z  uwzględnieniem  odprowadzania  ścieków  i  okresu  obliczeniowego 
wej lub nośników energii oraz wody z uwzględnieniem odprowadzania ścieków, cen jednostkowych energii końcowej lub 
nośnika energii oraz wody z uwzględnieniem odprowadzania ścieków i okresu obliczeniowego według wzoru:

według wzoru: 

gdzie:

gdzie: 

30  –  okres obliczeniowy,

30  – okres obliczeniowy, 

n

Cuz = 30 ∙ ∑(En ∙

k=1

 Cjn)

n  –  każdy kolejny rodzaj energii końcowej lub nośnika energii oraz wody z uwzględnieniem odprowadzania ścieków,

En  – 

ilość n-tej energii końcowej lub n-tego nośnika energii oraz wody zużywanej w ciągu roku z uwzględnieniem odpro-
wadzania ścieków,

1)  Minister  Rozwoju  i  Technologii  kieruje  działem  administracji  rządowej  –  budownictwo,  planowanie  
i zagospodarowanie przestrzenne oraz mieszkalnictwo, na podstawie § 1 ust. 2 pkt 1 rozporządzenia Prezesa 
Rady  Ministrów  z  dnia  27  października  2021  r.  w  sprawie  szczegółowego  zakresu  działania  Ministra 
Rozwoju i Technologii (Dz. U. poz. 1945). 

Cjn  –  cena jednostkowa n-tej energii końcowej lub n-tego nośnika energii oraz wody z uwzględnieniem odprowadzania 

ścieków.

1)  Minister Rozwoju i Technologii kieruje działem administracji rządowej – budownictwo, planowanie i zagospodarowanie przestrzenne 
oraz mieszkalnictwo, na podstawie § 1 ust. 2 pkt 1 rozporządzenia Prezesa Rady Ministrów z dnia 27 października 2021 r. w sprawie 
szczegółowego zakresu działania Ministra Rozwoju i Technologii (Dz. U. poz. 1945).

DZIENNIK USTAWRZECZYPOSPOLITEJ  POLSKIEJ 
                                                 
Dziennik Ustaw 

– 2 –  

Poz. 2276

2. Zamawiający określa w specyfikacji warunków zamówienia minimalną charakterystykę energetyczną budynku, ze 
wskazaniem maksymalnej ilości energii końcowej lub nośników energii zużywanych w ciągu roku, oraz maksymalne zużycie 
wody w ciągu roku z uwzględnieniem odprowadzania ścieków.

3. Dane, o których mowa w ust. 2, oblicza się zgodnie z przepisami wykonawczymi wydanymi na podstawie art. 15 
ustawy z dnia 29 sierpnia 2014 r. o charakterystyce energetycznej budynków (Dz. U. z 2021 r. poz. 497) w oparciu o przy-
jęte założenia wynikające z dokumentacji projektowej oraz specyfikacji technicznych wykonania i odbioru robót budowla-
nych lub programu funkcjonalno-użytkowego.

4. Zamawiający określa w specyfikacji warunków zamówienia ceny jednostkowe energii końcowej lub nośników energii 

oraz wody z uwzględnieniem odprowadzania ścieków, które zostaną ujęte w kalkulacji kosztów cyklu życia budynku.

5. Zamawiający określa ceny jednostkowe, o których mowa w ust. 4, na podstawie umów zawartych przez niego z do-
stawcami energii końcowej lub nośników energii oraz wody z uwzględnieniem odprowadzania ścieków lub taryfikatorów 
publikowanych przez dostawców energii końcowej lub nośników energii oraz wody.

6. Wykonawca przedstawia w ofercie przewidywaną ilość energii końcowej lub nośnika energii oraz wody z uwzględ-

nieniem odprowadzania ścieków zużywanych w ciągu roku.

7. W przypadku gdy podana przez wykonawcę w ofercie przewidywana ilość energii końcowej, nośnika energii lub 
wody z uwzględnieniem odprowadzania ścieków zużywanych w ciągu roku jest niższa od wartości maksymalnej wskazanej 
przez zamawiającego, wykonawca przedstawia w ofercie rozwiązania, których zastosowanie zapewni osiągnięcie wskaza-
nych przez niego wartości.

§ 4. 1. Koszty utrzymania wynikające z eksploatacji budynku, umożliwiające utrzymanie budynku w należytym stanie 
technicznym i estetycznym, oblicza się jako sumę jednostkowych kosztów utrzymania wyrobów w okresie obliczeniowym 
pomniejszonych o wartość gwarancji wykonawcy dla danego wyrobu według wzoru:

– 3 – 

gdzie:

i 

gdzie: 
–  każdy kolejny wyrób,
i 

– każdy kolejny wyrób, 

i

Cut = ∑(Ai −
k=1

 Bi)

Ai  –  koszt utrzymania i  -tego wyrobu w okresie obliczeniowym,
Bi  –  wartość gwarancji wykonawcy i  -tego wyrobu.

Ai  – koszt utrzymania i-tego wyrobu w okresie obliczeniowym,  

– wartość gwarancji wykonawcy i-tego wyrobu. 

Bi 
2. Koszt utrzymania i -tego wyrobu w okresie obliczeniowym oblicza się według wzoru:

  2.  Koszt  utrzymania  i-tego  wyrobu  w  okresie  obliczeniowym  oblicza  się  według 
Ai = I · K · N

wzoru: 

gdzie:

I 

liczba jednostek wyrobu,

– 
gdzie: 

K  –  koszt wymiany jednostki wyrobu,
– liczba jednostek wyrobu, 

I 
N  – 

liczba cykli użytkowania wyrobu w okresie obliczeniowym.

Ai = I · K · N  

– koszt wymiany jednostki wyrobu, 

K 
3. Wartość gwarancji wykonawcy i  -tego wyrobu oblicza się według wzoru:
N 

– liczba cykli użytkowania wyrobu w okresie obliczeniowym. 

3. Wartość gwarancji wykonawcy i-tego wyrobu oblicza się według wzoru: 

Bi = (Ai · Og / 30)

gdzie:

Og  –  okres gwarancji i -tego wyrobu wyrażony w latach.

Bi = (Ai · Og / 30) 

1) 

2) 

gdzie: 
4. Zamawiający określa w specyfikacji warunków zamówienia:
Og   – okres gwarancji i-tego wyrobu  wyrażony w latach. 
rodzaje wyrobów uwzględnianych przy ustalaniu kosztu utrzymania − biorąc pod uwagę ich wpływ na funkcjonowanie 
budynku oraz szacowany koszt wyrobów z uwzględnieniem montażu;

4. Zamawiający określa w specyfikacji warunków zamówienia: 

rodzaje  wyrobów  uwzględnianych  przy  ustalaniu  kosztu  utrzymania  −  biorąc  pod  

koszt wymiany jednostki wyrobu − na podstawie kalkulacji z wykorzystaniem analizy indywidualnej lub kosztoryso-
1) 
wych norm nakładów rzeczowych, zawierających jednostkowe nakłady rzeczowe oraz metodę interpolacji i ekstrapo-
lacji z uwzględnieniem cen jednostkowych, ustalonych na podstawie danych rynkowych.

uwagę  ich  wpływ  na  funkcjonowanie  budynku  oraz  szacowany  koszt  wyrobów  

z uwzględnieniem montażu; 

2) 

koszt  wymiany  jednostki  wyrobu  −  na  podstawie  kalkulacji  z  wykorzystaniem  analizy 

indywidualnej 

lub  kosztorysowych  norm  nakładów  rzeczowych,  zawierających 

jednostkowe 

nakłady 

rzeczowe 

oraz  metodę 

interpolacji 

i 

ekstrapolacji 

z uwzględnieniem cen jednostkowych, ustalonych na podstawie danych rynkowych. 

5. Zamawiający określa w specyfikacji warunków zamówienia liczbę cykli użytkowania 

wyrobu  w  okresie  obliczeniowym  zgodnie  z  załącznikiem  nr  1  do  rozporządzenia, 

uwzględniając funkcję budynku oraz przewidywany sposób jego użytkowania. 

6. Zamawiający ustala liczbę jednostek wyrobu na podstawie dokumentacji projektowej 

oraz  specyfikacji  technicznych  wykonania  i  odbioru  robót  budowlanych  albo  programu 

funkcjonalno-użytkowego 

lub 

innych  opracowań  w  przypadku 

realizacji  obiektu 

 
 
Dziennik Ustaw 

– 3 –  

Poz. 2276

5. Zamawiający określa w specyfikacji warunków zamówienia liczbę cykli użytkowania wyrobu w okresie obliczenio-
wym zgodnie z załącznikiem nr 1 do rozporządzenia, uwzględniając funkcję budynku oraz przewidywany sposób jego użyt-
kowania.

6. Zamawiający ustala liczbę jednostek wyrobu na podstawie dokumentacji projektowej oraz specyfikacji technicznych 
wykonania i odbioru robót budowlanych albo programu funkcjonalno-użytkowego lub innych opracowań w przypadku reali-
zacji obiektu budowlanego za pomocą dowolnych środków, zgodnie z wymaganiami określonymi przez zamawiającego.

7. Wykonawca określa w ofercie okres gwarancji dla poszczególnych wyrobów.

§ 5. Zamawiający sporządza kalkulację kosztów cyklu życia budynku na formularzu danych do określenia kosztów cyklu 

życia budynku, którego wzór jest określony w załączniku nr 2 do rozporządzenia.

§ 6. W rachunku kosztów cyklu życia budynku nie uwzględnia się podatku od towarów i usług.

§ 7. W przypadku zastosowania kalkulacji kosztów cyklu życia budynku w odniesieniu do części budynku w rozumie-
niu ustawy z dnia 29 sierpnia 2014 r. o charakterystyce energetycznej budynków koszty nabycia, użytkowania oraz utrzy-
mania oblicza się dla tej części.

§ 8. Rozporządzenie wchodzi w życie z dniem 1 stycznia 2022 r.2)

Minister Rozwoju i Technologii: P. Nowak

2)  Niniejsze rozporządzenie było poprzedzone rozporządzeniem Ministra Inwestycji i Rozwoju z dnia 11 lipca 2018 r. w sprawie meto-
dy kalkulacji kosztów cyklu życia budynków oraz sposobu przedstawiania informacji o tych kosztach (Dz. U. poz. 1357), które traci 
moc z dniem wejścia w życie niniejszego rozporządzenia zgodnie z art. 97 ustawy z dnia 11 września 2019 r. – Przepisy wprowadza-
jące ustawę – Prawo zamówień publicznych (Dz. U. poz. 2020 oraz z 2020 r. poz. 1086 i 2275).

 
Dziennik Ustaw 

– 4 –  

Załączniki do rozporządzenia Ministra 
Rozwoju i Technologii  
Załączniki do rozporządzenia Ministra Rozwoju i Technologii 
z dnia 23 listopada 2021 r. (poz. …..) 
z dnia 23 listopada 2021 r. (poz. 2276)

Poz. 2276

załącznik nr 1

Załącznik nr 1 

LICZBA CYKLI UŻYTKOWANIA WYROBU W OKRESIE OBLICZENIOWYM

LICZBA CYKLI UŻYTKOWANIA WYROBU W OKRESIE OBLICZENIOWYM 

Lp. 

Rodzaj wyrobu 

Liczba cykli użytkowania wyrobu  
w okresie obliczeniowym 

1 

2 

3 

Okna 

Drzwi 

Posadzki: 

a) terakota/gres 

b) panele 

c) parkiet 

d) inne* 

4  Wyroby wchodzące w skład instalacji: 

a) wodociągowej 

b) gazowej 

c) elektrycznej 

d) klimatyzacyjnej 

e) innej* 

Dźwigi 

Elewacja 

Pokrycia dachowe: 

a) blacha 

b) dachówki 

c) papa 

d) inne* 

5 

6 

7 

8 

Inne* 

* Określa zamawiający. 

1–4 

1–3 

1–2 

3–5 

1–3 

1–10 

1–3 

1–3 

1–3 

1–3 

1–10 

1–3 

1–2 

1–3 

1–2 

2–4 

1–10 

1–15 

 
 
 
 
 
 
Dziennik Ustaw 

– 5 –  
– 2 – 

WZÓR 
WZÓR

Poz. 2276

załącznik nr 2

Załącznik nr 2  

FORMULARZ DANYCH DO OKREŚLENIA KOSZTÓW CYKLU ŻYCIA BUDYNKU 

tabela 1. informacje podstawowe  

Nazwa nadana zamówieniu przez zamawiającego  

Wykonawca 

Zamawiający 

Data wypełnienia formularza 

tabela 2. Koszty nabycia  

Lp. 

1 

Rodzaje grup kosztów 

Koszty nabycia 

Brutto 

Netto  
(bez podatku od towarów i usług) 

Cena oferty (w złotych) 

tabela 3. Koszty użytkowania  

Lp. 

Rodzaje grup kosztów 

Rodzaj 
nośnika 
energii 

Ilość zużytej  
w ciągu roku 
energii końcowej/ 
nośnika 
energii/wody  
z uwzględnieniem 
odprowadzania 
ścieków1) 

Cena jednostkowa 
energii 
końcowej/nośnika 
energii/wody  
z uwzględnieniem 
odprowadzania 
ścieków  
(w złotych)2) 

Koszty  
roczne 
(w złotych) 

Koszty  
w całym 
okresie 
obliczeniowym 
(w złotych) 

1  Ogrzewanie budynku 

2 

Przygotowanie ciepłej 
wody użytkowej  

3 

Chłodzenie budynku 

4 

Zaopatrzenie w wodę  
z uwzględnieniem 
odprowadzania ścieków 

– 

5  Oświetlenie wbudowane 

6 

Zasilanie dźwigów 

SUMA 

1) Na podstawie oferty wykonawcy. 
2) Na podstawie specyfikacji warunków zamówienia, określanej przez zamawiającego.  

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
Dziennik Ustaw 

– 6 –  
– 3 – 

Poz. 2276

tabela 4. Koszty utrzymania  

Lp. 

Rodzaje grup kosztów 

Liczba 
jednostek 
wyrobu1) 

Okres gwarancji2) 

Koszt wymiany 
jednostki wyrobu 
(w złotych)3) 

Koszty   
w całym okresie 
obliczeniowym  
(w złotych) 

1 

Koszty utrzymania 
(wyroby określone 
przez zamawiającego) 

1.1  Wyrób 1  

1.2  Wyrób 2  

  … 

SUMA 

1) Na podstawie dokumentacji projektowej oraz specyfikacji technicznych wykonania i odbioru robót budowlanych albo 
programu funkcjonalno-użytkowego lub innych opracowań w przypadku realizacji obiektu budowlanego za pomocą 
dowolnych środków, zgodnie z wymaganiami określonymi przez zamawiającego. 

2) Na podstawie oferty wykonawcy. 
3) Na podstawie specyfikacji warunków zamówienia, określanej przez zamawiającego. 

tabela 5. suma kosztów cyklu życia budynku 

Lp. 

Rodzaje grup kosztów 

Koszty (w złotych) 

1  Koszty nabycia 

2  Koszty użytkowania 

3  Koszty utrzymania 

SUMA 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
DZIENNIK USTAW 
RZECZYPOSPOLITEJ POLSKIEJ 

Warszawa, dnia 29 grudnia 2021 r. 

Poz. 2454 

RO ZPO RZĄDZENIE  
M INISTRA RO ZWO J U I T ECH NO LO GII 1) 

z dnia 20 grudnia 2021 r. 

w sprawie szczegółowego zakresu i formy dokumentacji projektowej, specyfikacji technicznych wykonania 
i odbioru robót budowlanych oraz programu funkcjonalno-użytkowego 

Na  podstawie  art. 103 ust. 4  ustawy  z dnia  11 września  2019 r.  –  Prawo  zamówień  publicznych  (Dz. U.  z 2021 r. 

poz. 1129, 1598, 2054 i 2269) zarządza się, co następuje: 

Rozdział 1 

Przepisy ogólne 

§ 1. Ilekroć w rozporządzeniu jest mowa o: 

1) 

2) 

3) 

4) 

grupach, klasach, kategoriach robót  – należy przez to rozumieć grupy, klasy, kategorie określone w rozporządzeniu 
(WE) nr 2195/2002 Parlamentu Europejskiego i Rady z dnia 5 listopada 2002 r. w sprawie Wspólnego Słownika Za-
mówień  (CPV)  (Dz. Urz.  WE  L  340  z 16.12.2002,  str. 1,  z późn.  zm.2)  –  Dz. Urz.  UE  Polskie  wydanie  specjalne 
rozdz. 6, t. 5, str. 3, z późn. zm.), zwanym dalej „Wspólnym Słownikiem Zamówień”; 

pracach towarzyszących – należy przez to rozumieć prace niezbędne do wykonania robót podstawowych niezaliczane 
do robót tymczasowych, w tym wytyczanie geodezyjne i inwentaryzację powykonawczą; 

robotach podstawowych – należy przez to rozumieć minimalny zakres prac, które po wykonaniu są możliwe do ode-
brania pod względem ilości i wymogów jakościowych oraz uwzględniają przyjęty stopień zagregowania robót; 

robotach  tymczasowych  –  należy  przez  to  rozumieć  prace,  które  są projektowane i wykonywane jako potrzebne do 
wykonania robót podstawowych, ale nie są przekazywane zamawiającemu i są usuwane po wykonaniu robót podsta-
wowych. 

§ 2. 1.  Dokumentacja  projektowa,  specyfikacje  techniczne  wykonania  i odbioru  robót  budowlanych  oraz  program 
funkcjonalno-użytkowy są odrębnymi opracowaniami, w których wydziela się tomy zgodnie z przyjętą systematyką podzia-
łu robót budowlanych. 

2. Opracowania,  o których  mowa  w ust. 1,  mogą  być  sporządzone  w formie  papierowej  lub  w formie elektronicznej 

w postaci plików komputerowych w formacie PDF. 

3. W przypadku opracowania sporządzonego w formie papierowej wszystkie strony w każdym tomie są trwale spięte 

i ponumerowane. 

1)  Minister  Rozwoju  i Technologii  kieruje  działem  administracji  rządowej  –  budownictwo,  planowanie  i zagospodarowanie  prze-
strzenne oraz mieszkalnictwo, na podstawie § 1 ust. 2 pkt 1 rozporządzenia Prezesa Rady Ministrów z dnia 27 października 2021 r. 
w sprawie szczegółowego zakresu działania Ministra Rozwoju i Technologii (Dz. U. poz. 1945). 

2)  Zmiany wymienionego rozporządzenia zostały ogłoszone w Dz. Urz. UE L 329 z 17.12.2003, str. 1 – Dz. Urz. UE Polskie wydanie 

specjalne rozdz. 6, t. 6, str. 72, Dz. Urz. UE L 74 z 15.03.2008, str. 1 oraz Dz. Urz. UE L 188 z 18.07.2009, str. 14. 

  
 
 
                                                           
Dziennik Ustaw 

– 2 – 

Rozdział 2 

Zakres i forma dokumentacji projektowej 

Poz. 2454 

§ 3. Zakres dokumentacji projektowej ustala zamawiający, biorąc pod uwagę tryb udzielenia zamówienia publiczne-
go, zwanego dalej „zamówieniem”, oraz wymagania dotyczące postępowania poprzedzającego rozpoczęcie robót budow-
lanych wynikające z ustawy z dnia 7 lipca 1994 r. – Prawo budowlane (Dz. U. z 2021 r. poz. 2351). 

§ 4. 1.  Dokumentacja  projektowa  służąca  do  opisu  przedmiotu  zamówienia  na  wykonanie  robót  budowlanych,  dla 
których jest wymagane uzyskanie pozwolenia na budowę albo zgłoszenie robót budowlanych, do którego dołącza się pro-
jekt budowlany zgodnie z przepisami ustawy z dnia 7 lipca 1994 r. – Prawo budowlane, składa się w szczególności z: 

1) 

2) 

3) 

projektu budowlanego w zakresie uwzględniającym specyfikę robót budowlanych; 

projektu wykonawczego w zakresie, o którym mowa w § 5; 

przedmiaru robót w zakresie, o którym mowa w § 6. 

2. Dokumentacja projektowa służąca do opisu przedmiotu zamówienia na wykonanie robót budowlanych, dla których 
nie jest wymagane  uzyskanie pozwolenia na  budowę  albo zgłoszenie robót budowlanych, do którego dołącza się projekt 
budowlany zgodnie z przepisami ustawy z dnia 7 lipca 1994 r. – Prawo budowlane, składa się w szczególności z: 

1) 

2) 

3) 

planów, rysunków lub innych dokumentów umożliwiających jednoznaczne określenie rodzaju i zakresu robót podsta-
wowych oraz uwarunkowań i dokładnej lokalizacji ich wykonywania; 

przedmiaru robót w zakresie, o którym mowa w § 6; 

projektów, pozwoleń, uzgodnień i opinii wymaganych odrębnymi przepisami. 

3. Jeżeli zamówienie na roboty budowlane, o których mowa w ust. 1 i 2, jest udzielane w trybie zamówienia z wolnej 
ręki  lub  w projektowanych postanowieniach umowy przyjęto zasadę wynagrodzenia ryczałtowego, dokumentacja  projek-
towa może nie obejmować przedmiaru robót. 

§ 5. 1. Projekt wykonawczy stanowi uzupełnienie i uszczegółowienie projektu budowlanego w zakresie i stopniu do-
kładności  niezbędnych  do  sporządzenia  przedmiaru robót,  kosztorysu inwestorskiego, przygotowania oferty przez wyko-
nawcę i realizacji robót budowlanych. 

2. Projekt wykonawczy zawiera rysunki w skali uwzględniającej specyfikę zamawianych robót i zastosowanych skal 

rysunków w projekcie budowlanym wraz z wyjaśnieniami opisowymi, które dotyczą: 

1) 

2) 

3) 

4) 

części obiektu, 

rozwiązań budowlano-konstrukcyjnych i materiałowych, 

detali architektonicznych oraz urządzeń budowlanych, 

sieci uzbrojenia terenu, instalacji i wyposażenia technicznego 

– których odzwierciedlenie na rysunkach projektu budowlanego nie jest wystarczające dla potrzeb, o których mowa w ust. 1. 

3. Projekt wykonawczy, w zależności od zakresu i rodzaju robót budowlanych stanowiących przedmiot zamówienia, 

dotyczy: 

1) 

2) 

3) 

4) 

przygotowania terenu pod budowę; 

robót budowlanych w zakresie wznoszenia kompletnych obiektów budowlanych lub ich części oraz robót w zakresie 
inżynierii lądowej i wodnej, włącznie z robotami wykończeniowymi w zakresie obiektów budowlanych; 

robót w zakresie instalacji budowlanych; 

robót związanych z zagospodarowaniem terenu. 

4. Wymagania dotyczące formy projektu wykonawczego przyjmuje się odpowiednio jak dla projektu budowlanego. 

§ 6. 1. Przedmiar robót zawiera zestawienie przewidywanych do wykonania robót podstawowych w kolejności techno-
logicznej ich wykonania wraz z ich szczegółowym opisem lub ze wskazaniem podstaw ustalających szczegółowy opis oraz 
wraz ze wskazaniem właściwych specyfikacji technicznych wykonania i odbioru robót budowlanych, a także z obliczeniem 
i zestawieniem liczby jednostek przedmiarowych robót podstawowych. 

2. Przedmiar robót składa się z następujących elementów: 

1) 

2) 

3) 

strony tytułowej przedmiaru robót; 

spisu działów przedmiaru robót; 

tabeli przedmiaru robót. 

  
 
 
Dziennik Ustaw 

– 3 – 

Poz. 2454 

§ 7. 1. Strona tytułowa przedmiaru robót zawiera następujące informacje: 

nazwę nadaną zamówieniu przez zamawiającego; 

adres obiektu budowlanego, a w przypadku braku adresu – opis lokalizacji obiektu budowlanego; 

1) 

2) 

3)  w zależności od zakresu robót budowlanych objętych przedmiotem zamówienia – nazwy i kody: 

a)  grup robót, 

b)  klas robót, 

c)  kategorii robót; 

nazwę i adres zamawiającego; 

imię i nazwisko osoby opracowującej przedmiar robót oraz – o ile występują – nazwę i adres podmiotu opracowują-
cego przedmiar robót, oraz datę opracowania. 

4) 

5) 

2. Jeżeli objętość informacji, o których mowa w ust. 1, uniemożliwia zamieszczenie ich na stronie tytułowej, dopusz-

cza się zamieszczenie tych informacji na kolejnych stronach albo w postaci załącznika do strony tytułowej. 

§ 8. 1. Spis działów przedmiaru robót przedstawia podział wszystkich robót budowlanych w danym obiekcie na grupy 

robót zgodnie ze Wspólnym Słownikiem Zamówień. 

2. Dalszy podział przedmiaru robót należy opracować według systematyki ustalonej indywidualnie lub na podstawie 

systematyki stosowanej w publikacjach zawierających kosztorysowe normy nakładów rzeczowych. 

3. W przypadku robót budowlanych dotyczących wielu obiektów spisem działów przedmiaru robót należy objąć do-
datkowo  podział  całej  inwestycji  na  obiekty  budowlane.  Grupa  robót  dotycząca  przygotowania  terenu  stanowi  odrębny 
dział przedmiaru dla wszystkich obiektów. 

§ 9. 1. Tabela przedmiaru robót zawiera pozycje przedmiarowe odpowiadające robotom podstawowym. 

2. W tabeli przedmiaru robót nie uwzględnia się robót tymczasowych, z wyłączeniem przypadków, gdy istnieją uza-

sadnione podstawy do ich odrębnego rozliczania. 

§ 10. 1. Dla każdej pozycji przedmiaru robót należy podać następujące informacje: 

numer pozycji przedmiaru; 

kod pozycji przedmiaru, określony zgodnie z ustaloną indywidualnie systematyką robót lub na podstawie wskazanych 
publikacji zawierających kosztorysowe normy nakładów rzeczowych; 

numer  specyfikacji technicznej wykonania i odbioru robót budowlanych, zawierającej wymagania dla danej pozycji 
przedmiaru; 

nazwę i opis pozycji przedmiaru oraz obliczenia liczby jednostek miary dla pozycji przedmiarowej; 

jednostkę miary, której dotyczy pozycja przedmiaru; 

liczbę jednostek miary pozycji przedmiaru. 

1) 

2) 

3) 

4) 

5) 

6) 

2. Liczbę  jednostek  miary  podaną  w przedmiarze  oblicza  się  na  podstawie  rysunków  w dokumentacji  projektowej 

w sposób zgodny z zasadami podanymi w specyfikacjach technicznych wykonania i odbioru robót budowlanych. 

3. Obliczenia  liczby  jednostek  miary  –  jeżeli  nie  są  zamieszczone  w danej  pozycji  przedmiaru  –  dołącza  się  do 

przedmiaru robót. 

§ 11. 1.  Do  dokumentacji  projektowej,  o której  mowa  w § 4 ust. 2,  dołącza  się  stronę  tytułową,  która  zawiera 

w szczególności: 

1) 

2) 

nazwę nadaną zamówieniu przez zamawiającego; 

adres obiektu budowlanego, którego dotyczy dokumentacja projektowa, a w przypadku braku adresu – opis lokaliza-
cji obiektu budowlanego; 

3)  w zależności od zakresu robót budowlanych objętych przedmiotem zamówienia – nazwy i kody: 

a)  grup robót, 

b)  klas robót, 

c)  kategorii robót; 

  
 
 
Dziennik Ustaw 

– 4 – 

Poz. 2454 

4) 

5) 

6) 

nazwę i adres zamawiającego; 

spis zawartości dokumentacji projektowej; 

imię  i nazwisko  osoby  opracowującej  części  składowe  dokumentacji  projektowej  oraz  –  o ile  występują  –  nazwę 
i adres podmiotu, oraz datę opracowania. 

2. Jeżeli objętość informacji, o których mowa w ust. 1, uniemożliwia zamieszczenie ich na stronie tytułowej, dopusz-

cza się zamieszczenie tych informacji na kolejnych stronach albo w postaci załącznika do strony tytułowej. 

Zakres i forma specyfikacji technicznych wykonania i odbioru robót budowlanych 

Rozdział 3 

§ 12. Specyfikacje  techniczne  wykonania  i odbioru  robót  budowlanych  stanowią  opracowania  zawierające  w  szcze-
gólności zbiory wymagań, które są niezbędne do określenia standardu i jakości wykonania robót, w zakresie sposobu wy-
konania  robót  budowlanych,  właściwości  wyrobów  budowlanych  oraz  oceny  prawidłowości  wykonania  poszczególnych 
robót. 

§ 13. 1.  Specyfikacje  techniczne  wykonania  i odbioru  robót  budowlanych,  w zależności  od  stopnia  skomplikowania 
robót  budowlanych,  składają  się ze specyfikacji technicznych wykonania i odbioru robót podstawowych, rodzajów robót 
według przyjętej systematyki lub grup robót. 

2. Specyfikacje techniczne wykonania i odbioru robót budowlanych dla budowy w rozumieniu ustawy z dnia 7 lipca 
1994 r. – Prawo budowlane należy opracować z uwzględnieniem podziału grup robót według Wspólnego Słownika Zamó-
wień, określając w nich co najmniej: 

1) 

2) 

3) 

4) 

roboty budowlane w zakresie przygotowania terenu pod budowę; 

roboty budowlane w zakresie wznoszenia kompletnych obiektów budowlanych lub ich części oraz roboty w zakresie 
inżynierii lądowej i wodnej; 

roboty w zakresie instalacji budowlanych; 

roboty wykończeniowe w zakresie obiektów budowlanych. 

3. Wspólne wymagania dotyczące robót budowlanych objętych przedmiotem zamówienia mogą być ujęte w ogólnej 

specyfikacji technicznej wykonania i odbioru robót budowlanych. 

4. Podział grup robót,  o którym mowa w ust. 2, stosuje  się odpowiednio do robót budowlanych polegających na re-

moncie obiektu budowlanego. 

§ 14. 1. Specyfikacje techniczne wykonania i odbioru robót budowlanych zawierają co najmniej: 

1) 

stronę tytułową, która obejmuje: 

a)  nazwę nadaną zamówieniu przez zamawiającego, 

b)  adres  obiektu  budowlanego,  którego  dotyczy  specyfikacja  techniczna  wykonania  i odbioru  robót  budowlanych, 

a w przypadku braku adresu – opis lokalizacji obiektu, 

c)  w zależności od zakresu robót budowlanych objętych przedmiotem zamówienia – nazwy i kody: 

–  grup robót, 

–  klas robót, 

–  kategorii robót, 

d)  nazwę i adres zamawiającego, 

e)  nazwę specyfikacji technicznej wykonania odbioru robót budowlanych i jej numer, 

f) 

imię  i nazwisko  osoby  opracowującej  specyfikację  techniczną  wykonania  i odbioru  robót  budowlanych  oraz  – 
o ile  występują  –  nazwę  i adres  podmiotu  opracowującego  specyfikację  techniczną  wykonania  i odbioru  robót 
budowlanych; 

2) 

część ogólną, która obejmuje: 

a)  przedmiot i zakres robót budowlanych, 

b)  wyszczególnienie i opis prac towarzyszących i robót tymczasowych, 

  
 
 
Dziennik Ustaw 

– 5 – 

Poz. 2454 

c) 

informacje o terenie budowy zawierające wszystkie niezbędne dane istotne z uwagi na: 

–  organizację robót budowlanych, 

–  zabezpieczenie interesów osób trzecich, 

–  ochronę środowiska, 

–  warunki bezpieczeństwa pracy, 

–  zaplecze dla potrzeb wykonawcy, 

–  warunki organizacji ruchu, 

–  ogrodzenie, 

–  zabezpieczenie chodników i jezdni, 

d)  określenia podstawowe, zawierające definicje pojęć i określeń nigdzie wcześniej niezdefiniowanych, a wymaga-
jących zdefiniowania w celu jednoznacznego rozumienia zapisów dokumentacji projektowej i specyfikacji tech-
nicznej wykonania i odbioru robót budowlanych; 

3)  wymagania dotyczące właściwości wyrobów budowlanych oraz niezbędne wymagania związane z ich przechowywa-
niem,  transportem,  warunkami  dostawy,  składowaniem  i kontrolą  jakości  –  poszczególne  wymagania  odnosi  się  do 
postanowień norm; 

4)  wymagania  dotyczące  sprzętu  i maszyn  niezbędnych  lub  zalecanych  do  wykonania  robót  budowlanych  zgodnie 

z założoną jakością; 

5)  wymagania dotyczące środków transportu; 

6)  wymagania dotyczące wykonania robót budowlanych z podaniem sposobu wykończenia poszczególnych elementów, 
tolerancji wymiarowych i szczegółów technologicznych oraz niezbędne informacje dotyczące odcinków robót budow-
lanych, przerw i ograniczeń, a także wymagania specjalne; 

7) 

opis działań związanych z kontrolą, badaniami oraz odbiorem wyrobów i robót budowlanych w nawiązaniu do doku-
mentów odniesienia; 

8)  wymagania dotyczące przedmiaru i obmiaru robót; 

9) 

opis sposobu odbioru robót budowlanych; 

10)  opis sposobu rozliczenia robót tymczasowych i prac towarzyszących; 

11)  dokumenty odniesienia – dokumenty będące podstawą do wykonania robót budowlanych, w tym wszystkie elementy 

dokumentacji projektowej, normy oraz inne dokumenty i ustalenia techniczne. 

2. Jeżeli  objętość  informacji,  o których  mowa  w ust. 1 pkt 1,  uniemożliwia  zamieszczenie  ich  na  stronie  tytułowej, 

dopuszcza się zamieszczenie tych informacji na kolejnych stronach albo w postaci załącznika do strony tytułowej. 

Rozdział 4 

Zakres i forma programu funkcjonalno-użytkowego 

§ 15. Program funkcjonalno-użytkowy służy do opisu przedmiotu zamówienia, ustalenia planowanych kosztów prac 
projektowych i robót budowlanych, przygotowania oferty – szczególnie w zakresie obliczenia ceny oferty oraz wykonania 
prac projektowych. 

§ 16. Program funkcjonalno-użytkowy składa się z następujących elementów: 

1) 

2) 

3) 

1) 

2) 

strony tytułowej; 

części opisowej; 

części informacyjnej. 

§ 17. 1. Strona tytułowa programu funkcjonalno-użytkowego obejmuje: 

nazwę nadaną zamówieniu przez zamawiającego; 

adres  obiektu  budowlanego,  którego  dotyczy  program  funkcjonalno-użytkowy,  a w przypadku  braku  adresu  –  opis 
lokalizacji obiektu budowlanego; 

  
 
 
Dziennik Ustaw 

– 6 – 

Poz. 2454 

3)  w zależności od zakresu robót budowlanych objętych przedmiotem zamówienia – nazwy i kody: 

a)  grup robót, 

b)  klas robót, 

c)  kategorii robót; 

nazwę i adres zamawiającego; 

spis zawartości programu funkcjonalno-użytkowego; 

imię  i nazwisko  osoby  opracowującej  program  funkcjonalno-użytkowy  oraz  –  o ile  występują  –  nazwę  i adres  pod-
miotu opracowującego program funkcjonalno-użytkowy. 

4) 

5) 

6) 

2. Jeżeli objętość informacji, o których mowa w ust. 1, uniemożliwia zamieszczenie ich na stronie tytułowej, dopusz-

cza się zamieszczenie tych informacji na kolejnych stronach albo w postaci załącznika do strony tytułowej. 

1) 

2) 

1) 

2) 

3) 

4) 

§ 18. 1. Część opisowa programu funkcjonalno-użytkowego obejmuje: 

opis ogólny przedmiotu zamówienia; 

opis wymagań zamawiającego w stosunku do przedmiotu zamówienia. 

2. Opis ogólny przedmiotu zamówienia obejmuje: 

charakterystyczne parametry określające wielkość obiektu lub zakres robót budowlanych; 

aktualne uwarunkowania wykonania przedmiotu zamówienia; 

ogólne właściwości funkcjonalno-użytkowe; 

szczegółowe  właściwości  funkcjonalno-użytkowe  wyrażone  we  wskaźnikach  powierzchniowo-kubaturowych,  ustalone 
zgodnie  z najnowszą  opublikowaną  w języku  polskim  Polską  Normą  PN-ISO  9836 „Właściwości  użytkowe w budow-
nictwie. Określanie i obliczanie wskaźników powierzchniowych i kubaturowych”, jeżeli wymaga tego specyfika obiektu 
budowlanego, w szczególności: 

a)  powierzchnie użytkowe poszczególnych pomieszczeń wraz z określeniem ich funkcji, 
b)  wskaźniki  powierzchniowo-kubaturowe,  w tym  wskaźnik  określający  udział  powierzchni  ruchu  w powierzchni 

netto, 
inne powierzchnie, jeżeli nie są pochodną powierzchni użytkowej opisanych wcześniej wskaźników, 

c) 
d)  określenie wielkości możliwych przekroczeń lub pomniejszenia przyjętych parametrów powierzchni i kubatur lub 

wskaźników. 

3. W przypadku budynków, w odniesieniu do szczegółowych właściwości funkcjonalno-użytkowych wyrażonych we 
wskaźnikach powierzchniowo-kubaturowych, o których mowa w ust. 2 pkt 4, uwzględnia się wymagania zawarte w przepi-
sach wydanych na podstawie art. 34 ust. 6 pkt 1 ustawy z dnia 7 lipca 1994 r. – Prawo budowlane. 

4. Wymagania zamawiającego w stosunku do przedmiotu zamówienia określa się, podając odpowiednio, w zależności 

od specyfiki obiektu budowlanego, wymagania dotyczące: 

1) 

2) 

3) 

4) 

przygotowania terenu budowy; 

architektury; 

konstrukcji; 

instalacji budowlanych; 

5)  wykończenia; 

6) 

zagospodarowania terenu. 

5. Opis wymagań, o których mowa w ust. 4, obejmuje: 

1) 

cechy obiektu dotyczące rozwiązań budowlano-konstrukcyjnych i wskaźników ekonomicznych; 

2)  warunki wykonania i odbioru robót budowlanych odpowiadających zawartości specyfikacji technicznych wykonania 

i odbioru robót budowlanych, o których mowa w rozdziale 3. 

§ 19. Część informacyjna programu funkcjonalno-użytkowego obejmuje: 

1) 

2) 

dokumenty potwierdzające zgodność zamierzenia budowlanego z wymaganiami wynikającymi z odrębnych przepisów; 

oświadczenie zamawiającego o posiadanym prawie do dysponowania nieruchomością na cele budowlane; 

  
 
 
Dziennik Ustaw 

– 7 – 

Poz. 2454 

3)  wskazanie przepisów prawnych i norm związanych z projektowaniem i wykonaniem zamierzenia budowlanego; 

4) 

inne posiadane informacje i dokumenty niezbędne do zaprojektowania robót budowlanych, w szczególności: 

a)  kopię mapy zasadniczej, 

b)  wyniki badań gruntowo-wodnych, 

c)  zalecenia konserwatorskie konserwatora zabytków, 

d) 

inwentaryzację zieleni, 

e)  dane dotyczące zanieczyszczeń atmosfery niezbędne do analizy ochrony powietrza oraz posiadane raporty, opinie 

lub ekspertyzy z zakresu ochrony środowiska, 

f)  pomiary ruchu drogowego, hałasu i innych uciążliwości, 

g) 

inwentaryzację lub dokumentację obiektów budowlanych, jeżeli podlegają one przebudowie, odbudowie, rozbu-
dowie, nadbudowie, rozbiórkom lub remontom w zakresie architektury, konstrukcji, instalacji i urządzeń techno-
logicznych, a także wskazania zamawiającego dotyczące urządzeń naziemnych i podziemnych przewidzianych do 
zachowania oraz obiektów przewidzianych do rozbiórki i ewentualne uwarunkowania rozbiórek, 

h)  porozumienia,  zgody  lub  pozwolenia  oraz  warunki  techniczne  i realizacyjne  związane  z przyłączeniem  obiektu 
do istniejących sieci wodociągowych, kanalizacyjnych, cieplnych, gazowych, energetycznych i teletechnicznych 
oraz dróg publicznych, kolejowych lub wodnych, 

i)  dodatkowe wytyczne inwestorskie i uwarunkowania związane z budową i jej przeprowadzeniem. 

§ 20. Przepisu § 19 pkt 2 nie stosuje się do zamówień na roboty budowlane dotyczące inwestycji w zakresie: 

1) 

2) 

3) 

4) 

5) 

6) 

7) 

8) 

9) 

linii  kolejowych,  linii  metra  i linii  tramwajowych realizowanych na podstawie rozdziału 2b ustawy z dnia 28 marca 
2003 r. o transporcie kolejowym (Dz. U. z 2021 r. poz. 1984); 

dróg publicznych realizowanych na podstawie ustawy z dnia 10 kwietnia 2003 r. o szczególnych zasadach przygoto-
wania i realizacji inwestycji w zakresie dróg publicznych (Dz. U. z 2020 r. poz. 1363 oraz z 2021 r. poz. 784 i 1228); 

lotnisk  użytku  publicznego  realizowanych  na  podstawie  ustawy  z dnia  12 lutego  2009 r.  o szczególnych  zasadach 
przygotowania i realizacji inwestycji w zakresie lotnisk użytku publicznego (Dz. U. z 2021 r. poz. 1079); 

budowli  przeciwpowodziowych  realizowanych  na  podstawie  ustawy  z dnia  8 lipca  2010 r.  o szczególnych  zasadach 
przygotowania do realizacji inwestycji w zakresie budowli przeciwpowodziowych (Dz. U. z 2021 r. poz. 1812); 

regionalnych sieci szerokopasmowych realizowanych na podstawie ustawy z dnia 7 maja 2010 r. o wspieraniu rozwo-
ju usług i sieci telekomunikacyjnych (Dz. U. z 2021 r. poz. 777, 784 i 2333); 

terminalu lub inwestycji towarzyszących przygotowywanych i realizowanych na podstawie ustawy z dnia 24 kwietnia 
2009 r.  o inwestycjach  w zakresie  terminalu  regazyfikacyjnego  skroplonego  gazu  ziemnego  w Świnoujściu  (Dz. U. 
z 2021 r. poz. 1836); 

sieci przesyłowych przygotowywanych i realizowanych na podstawie ustawy z dnia 24 lipca 2015 r. o przygotowaniu 
i realizacji strategicznych inwestycji w zakresie sieci przesyłowych (Dz. U. z 2021 r. poz. 428, 784 i 922); 

zadań inwestycyjnych określonych w Programie Inwestycji Organizacji Traktatu Północnoatlantyckiego w Dziedzinie 
Bezpieczeństwa  (NSIP),  realizowanych  na  podstawie  ustawy  z dnia  25 maja  2001 r.  o przebudowie  i modernizacji 
technicznej oraz finansowaniu Sił Zbrojnych Rzeczypospolitej Polskiej (Dz. U. z 2021 r. poz. 1221 i 1535); 

Inwestycji  i Inwestycji  Towarzyszących  realizowanych  na  podstawie  ustawy  z dnia  10 maja  2018 r.  o Centralnym 
Porcie Komunikacyjnym (Dz. U. z 2021 r. poz. 1354); 

10)  strategicznych inwestycji w sektorze naftowym realizowanych na podstawie ustawy z dnia 22 lutego 2019 r. o przygo-

towaniu i realizacji strategicznych inwestycji w sektorze naftowym (Dz. U. z 2021 r. poz. 1902); 

11)  inwestycji  w zakresie  budowy  portu  zewnętrznego  realizowanych  na  podstawie  ustawy  z dnia  9 sierpnia  2019 r. 

o inwestycjach w zakresie budowy portów zewnętrznych (Dz. U. z 2021 r. poz. 1853 i 2368). 

Rozdział 5 

Przepis przejściowy i przepis końcowy 

§ 21. 1.  Do  postępowań  o udzielenie  zamówienia  wszczętych  i niezakończonych  przed  dniem  1 stycznia  2022 r., 

w zakresie opisu przedmiotu zamówienia, stosuje się przepisy dotychczasowe. 

  
 
 
Dziennik Ustaw 

– 8 – 

Poz. 2454 

2. W celu przygotowania lub przeprowadzenia postępowania o udzielenie zamówienia wszczynanego po dniu 31 grudnia 
2021 r.  zamawiający  może  korzystać  z dokumentacji  projektowej,  specyfikacji  technicznych  wykonania  i odbioru  robót 
budowlanych lub programu funkcjonalno-użytkowego, sporządzonych przed dniem wejścia w życie rozporządzenia, o ile 
w dacie ich sporządzenia spełniały one wymagania określone w rozporządzeniu Ministra Infrastruktury z dnia 2 września 
2004 r.  w sprawie  szczegółowego  zakresu  i formy  dokumentacji  projektowej,  specyfikacji  technicznych  wykonania  i  od-
bioru robót budowlanych oraz programu funkcjonalno-użytkowego (Dz. U. z 2013 r. poz. 1129). 

§ 22. Rozporządzenie wchodzi w życie z dniem 1 stycznia 2022 r.3) 

Minister Rozwoju i Technologii: P. Nowak 

3)  Niniejsze rozporządzenie było poprzedzone rozporządzeniem Ministra Infrastruktury z dnia 2 września 2004 r. w sprawie szczegó-
łowego zakresu i formy dokumentacji projektowej, specyfikacji technicznych wykonania i odbioru robót budowlanych oraz progra-
mu  funkcjonalno-użytkowego  (Dz. U.  z 2013 r.  poz. 1129),  które  traci  moc  z dniem  wejścia  w życie  niniejszego  rozporządzenia 
zgodnie  z art. 97  ustawy  z dnia  11 września  2019 r.  ‒  Przepisy  wprowadzające  ustawę  ‒  Prawo  zamówień  publicznych  (Dz. U. 
poz. 2020 oraz z 2020 r. poz. 1086 i 2275). 

  
 
 
                                                           
DZIENNIK USTAW 
RZECZYPOSPOLITEJ POLSKIEJ 

Warszawa, dnia 29 grudnia 2021 r. 

Poz. 2458 

RO ZPO RZĄDZENIE  
M INISTRA RO ZWO J U I T ECH NO LO GII 1) 

z dnia 20 grudnia 2021 r. 

w sprawie określenia metod i podstaw sporządzania kosztorysu inwestorskiego, obliczania planowanych  
kosztów prac projektowych oraz planowanych kosztów robót budowlanych określonych w programie 
funkcjonalno-użytkowym 

Na  podstawie  art. 34 ust. 2  ustawy  z dnia  11 września  2019 r.  –  Prawo  zamówień  publicznych  (Dz. U.  z 2021 r. 

poz. 1129, 1598, 2054 i 2269) zarządza się, co następuje: 

Rozdział 1 

Przepisy ogólne 

§ 1. Ilekroć w rozporządzeniu jest mowa o: 

1) 

2) 

3) 

4) 

5) 

6) 

cenie  jednostkowej  –  należy  przez  to  rozumieć  sumę  kosztów  bezpośredniej  robocizny,  materiałów  i pracy  sprzętu 
oraz kosztów pośrednich i zysku, obliczoną na jednostkę przedmiarową robót podstawowych; 

jednostkowych nakładach rzeczowych – należy przez to rozumieć nakłady rzeczowe robocizny, materiałów i sprzętu 
niezbędne do wykonania jednostki przedmiarowej roboty podstawowej; 

katalogach – należy przez to rozumieć publikacje zawierające jednostkowe nakłady rzeczowe; 

kosztach pośrednich  – należy przez to rozumieć składnik kalkulacyjny wartości kosztorysowej, uwzględniający nie-
ujęte w kosztach bezpośrednich koszty zaliczane zgodnie z odrębnymi przepisami do kosztów uzyskania przychodów, 
w szczególności koszty ogólne budowy oraz koszty zarządu; 

pracach projektowych – należy przez to rozumieć zakres prac projektowych określony przez zamawiającego, z uwzględ-
nieniem odrębnych przepisów, trybu udzielenia zamówienia i specyfiki robót budowlanych; 

przedmiarze  robót  –  należy  przez  to  rozumieć  dokument  zawierający  zestawienie  przewidywanych  do  wykonania 
robót  podstawowych  w kolejności  technologicznej  ich  wykonania,  wraz  z ich szczegółowym opisem, miejscem wy-
konania  lub  wskazaniem  podstaw  ustalających  szczegółowy  opis,  z obliczeniem  i zestawieniem  liczby  jednostek 
przedmiarowych robót podstawowych; 

7) 

robotach podstawowych – należy przez to rozumieć minimalny zakres prac, które po wykonaniu są możliwe do ode-
brania pod względem ilości i wymogów jakościowych oraz uwzględniają przyjęty stopień zagregowania robót; 

8)  wartości kosztorysowej robót – należy przez to rozumieć wartość wynikającą z kosztorysu inwestorskiego stanowiącą 

podstawę ustalenia wartości zamówienia; 

9) 

założeniach wyjściowych do kosztorysowania – należy przez to rozumieć dane techniczne, technologiczne i organiza-
cyjne  nieokreślone  w dokumentacji  projektowej  oraz  specyfikacji  technicznej  wykonania  i odbioru  robót  budowla-
nych, a mające wpływ na wartość kosztorysową robót. 

1)  Minister  Rozwoju  i Technologii  kieruje  działem  administracji  rządowej  –  budownictwo,  planowanie  i zagospodarowanie  prze-
strzenne oraz mieszkalnictwo, na podstawie § 1 ust. 2 pkt 1 rozporządzenia Prezesa Rady Ministrów z dnia 27 października 2021 r. 
w sprawie szczegółowego zakresu działania Ministra Rozwoju i Technologii (Dz. U. poz. 1945). 

                  
 
 
                                                           
Dziennik Ustaw 

– 2 – 

Rozdział 2 

Poz. 2458 

Metody i podstawy sporządzania kosztorysu inwestorskiego 

§ 2. 1. Kosztorys inwestorski sporządza się metodą kalkulacji uproszczonej, polegającą na obliczeniu wartości kosz-
torysowej robót objętych przedmiarem robót jako sumy iloczynów liczby jednostek przedmiarowych robót podstawowych 
i cen jednostkowych robót podstawowych bez podatku od towarów i usług, według wzoru: 

Wk = ∑ (Lj × Cj) 

w którym poszczególne symbole oznaczają: 

Wk  – wartość kosztorysową robót; 

Lj 

Cj 

– liczbę jednostek przedmiarowych robót podstawowych; 

– cenę jednostkową roboty podstawowej. 

2. Wartość kosztorysowa robót obejmuje wartość wszystkich materiałów, urządzeń i konstrukcji potrzebnych do zreali-

zowania przedmiotu zamówienia. 

1) 

2) 

3) 

4) 

1) 

§ 3. 1. Podstawę do sporządzania kosztorysu inwestorskiego stanowią: 

dokumentacja projektowa; 

specyfikacja techniczna wykonania i odbioru robót budowlanych; 

założenia wyjściowe do kosztorysowania; 

ceny jednostkowe robót podstawowych. 

2. Przy ustalaniu cen jednostkowych robót podstawowych stosuje się w kolejności: 

ceny jednostkowe robót podstawowych określone na podstawie danych rynkowych, w tym danych z zawartych wcześniej 
umów lub powszechnie stosowanych aktualnych publikacji; 

2) 

kalkulacje szczegółowe cen jednostkowych. 

§ 4. 1. Kalkulacja szczegółowa ceny jednostkowej polega na określeniu wartości poszczególnych jednostkowych na-

kładów rzeczowych (kosztów bezpośrednich) oraz doliczeniu narzutów kosztów pośrednich i zysku, według wzoru: 

Cj = ∑ (n × c) + Kpj + Zj 

w którym poszczególne symbole oznaczają: 

Cj 

n 

c 

– cenę jednostkową roboty podstawowej; 

– jednostkowe nakłady rzeczowe: robocizny – nr, materiałów – nm, pracy sprzętu – ns; 

– cenę czynników produkcji: robocizny – Cr, ceny materiałów – Cm, ceny pracy sprzętu – Cs; 

n × c – koszty bezpośrednie jednostki przedmiarowej robót, według wzoru: 

n × c = (∑ nr × Cr + ∑ nm × Cm + ∑ ns × Cs); 

Kpj  – koszty pośrednie na jednostkę przedmiarową robót; 

Zj 

– zysk kalkulacyjny na jednostkę przedmiarową robót. 

2. Koszty pośrednie na  jednostkę przedmiarową robót ustala się za pomocą wskaźnika narzutu kosztów pośrednich, 

według wzoru: 

Kpj =

Wkp × (Rj + Sj)
100%

w którym poszczególne symbole oznaczają: 

Kpj  – koszty pośrednie na jednostkę przedmiarową robót; 

Wkp  – wskaźnik narzutu kosztów pośrednich w %; 

Rj 

Sj 

– koszt robocizny na jednostkę przedmiarową robót; 

– koszt pracy sprzętu na jednostkę przedmiarową robót. 

      
 
 
 
Dziennik Ustaw 

– 3 – 

Poz. 2458 

3. Zysk kalkulacyjny na jednostkę przedmiarową robót oblicza się jako iloczyn wskaźnika narzutu zysku i podstawy 

jego naliczania. 

1) 

2) 

1) 

2) 

§ 5. 1. Przy ustalaniu jednostkowych nakładów rzeczowych stosuje się w kolejności: 

analizę indywidualną; 

kosztorysowe normy nakładów rzeczowych określone w odpowiednich katalogach oraz metodę interpolacji i ekstra-
polacji, przy wykorzystaniu wielkości określonych w katalogach. 

2. Przy ustalaniu stawek i cen czynników produkcji stosuje się w kolejności: 

analizę własną; 

dane rynkowe lub powszechnie stosowane, aktualne publikacje. 

3. Ceny materiałów podaje się łącznie z kosztami zakupu. 

4. Przy ustalaniu wskaźnika narzutów kosztów pośrednich i wskaźnika narzutu zysku przyjmuje się wielkości określone 
według danych rynkowych, w tym danych z zawartych wcześniej umów lub powszechnie stosowanych aktualnych publikacji, 
a w przypadku braku takich danych – według analizy indywidualnej. 

5. Podstawę naliczania narzutu zysku ustala się w założeniach wyjściowych do kosztorysowania. 

§ 6. 1. Jednostkowe nakłady rzeczowe ustalone na podstawie analizy indywidualnej uwzględniają w przypadku: 

1) 

robocizny  –  liczbę  roboczogodzin  dotyczącą  wszystkich  czynności,  które  są  wymienione  w szczegółowych  opisach 
robót podstawowych wyszczególnionych pozycji kosztorysowych, oraz 5% rezerwy na czynności pomocnicze; 

2)  materiałów  –  ilości/liczby  wyszczególnionych  rodzajów  materiałów,  wyrobów  lub  prefabrykatów  niezbędnych  do 
wykonania robót podstawowych wyszczególnionych pozycji kosztorysowych, z uwzględnieniem ubytków i odpadów 
w transporcie i w procesie wbudowania; 

3) 

pracy sprzętu – liczbę maszynogodzin pracy wymienionych jednostek sprzętowych, niezbędnych do wykonania robót 
podstawowych  wyszczególnionych  pozycji  kosztorysowych,  z uwzględnieniem  przestojów  wynikających  z procesu 
technologicznego. 

2. Godzinowe  stawki  robocizny  kosztorysowej  ustalone  na  podstawie  analizy  własnej  obejmują  wszystkie składniki 

zaliczane do wynagrodzenia oraz koszty pochodne naliczane od wynagrodzeń, a w szczególności: 

1)  wynagrodzenie zasadnicze; 

2) 

3) 

4) 

5) 

6) 

premie regulaminowe; 

dodatkowe  składniki  wynagrodzenia  (dodatki  za  staż  pracy,  inne  dodatki  przysługujące  zgodnie  z postanowieniami 
regulaminu pracy); 

inne świadczenia związane z pracą (wynagrodzenia za czas urlopu wypoczynkowego i za czas innej usprawiedliwio-
nej nieobecności w pracy, zasiłki za czas niezdolności do pracy wskutek choroby, odprawy emerytalne, nagrody jubi-
leuszowe); 

obligatoryjne obciążenia płac; 

odpisy na zakładowy fundusz świadczeń socjalnych. 

3. W cenach jednostkowych materiałów ustalonych na podstawie analizy własnej nie uwzględnia się podatku od towa-

rów i usług. 

4. W cenach  jednostkowych  maszynogodzin  pracy  jednostek  sprzętowych  ustalonych  na  podstawie  analizy  własnej 

nie uwzględnia się podatku od towarów i usług. 

5. W cenach  jednostkowych  uwzględnia  się  kosztorysową  cenę  pracy  jednostki  sprzętowej  lub  transportowej  wraz 
z kosztami  obsługi  etatowej  oraz  koszty  jednorazowe,  uwzględniające  koszty  przewozu  sprzętu  lub  środków  transportu 
z bazy na budowę i z powrotem, montaż i demontaż na miejscu pracy albo przezbrojenie. 

§ 7. Kosztorys inwestorski obejmuje: 

1) 

stronę tytułową zawierającą: 

a)  nazwę nadaną zamówieniu przez zamawiającego, 

b) 

lokalizację obiektu budowlanego lub robót budowlanych, 

      
 
 
Dziennik Ustaw 

– 4 – 

Poz. 2458 

c)  nazwy i kody określone w rozporządzeniu (WE) nr 2195/2002 Parlamentu Europejskiego i Rady z dnia 5 listo-
pada 2002 r. w sprawie Wspólnego Słownika Zamówień (CPV) (Dz. Urz. WE L 340 z 16.12.2002, str. 1, z późn. 
zm.2)  –  Dz.  Urz.  UE  Polskie  wydanie  specjalne,  rozdz.  6,  t.  5,  str.  3,  z  późn.  zm.),  zwanym  dalej  „Wspólnym 
Słownikiem Zamówień”, 

d)  nazwę i adres zamawiającego, 

e) 

imię  i nazwisko  osoby  opracowującej  kosztorys  oraz  nazwę  i adres  podmiotu  opracowującego  kosztorys,  o ile 
występuje, 

f)  wartość kosztorysową robót, 

g)  datę opracowania kosztorysu; 

2) 

3) 

4) 

5) 

ogólną  charakterystykę  obiektu  lub  robót,  zawierającą  krótki  opis  techniczny  wraz  z istotnymi  parametrami,  które 
określają wielkość obiektu lub robót; 

przedmiar robót; 

kalkulację uproszczoną; 

tabelę  wartości elementów scalonych, sporządzoną w postaci sumarycznego zestawienia wartości robót określonych 
przedmiarem  robót,  łącznie  z narzutami  kosztów  pośrednich  i zysku,  odniesionych  do  elementu  obiektu  lub  zbior-
czych rodzajów robót; 

6) 

załączniki określające: 

a)  założenia wyjściowe do kosztorysowania, 

b)  kalkulacje szczegółowe cen jednostkowych, analizy indywidualne nakładów rzeczowych oraz analizy własne cen 

czynników produkcji i wskaźników narzutów kosztów pośrednich i zysku. 

Metody i podstawy obliczania planowanych kosztów robót budowlanych 

Rozdział 3 

§ 8. 1. Planowane koszty robót budowlanych oblicza się metodą wskaźnikową, jako sumę iloczynów wskaźnika ce-

nowego i liczby jednostek odniesienia, według wzoru: 

WRB = ∑ (WCi × ni) 

w którym poszczególne symbole oznaczają: 

WRB – wartość planowanych kosztów robót budowlanych; 

WCi  – wskaźnik cenowy i-tego składnika kosztów; 

ni 

– liczbę jednostek odniesienia dla i-tego składnika kosztów. 

2. Podstawę obliczenia planowanych kosztów robót budowlanych stanowią: 

1) 

program funkcjonalno-użytkowy; 

2)  wskaźniki cenowe. 

3. Składniki kosztów ustala się z uwzględnieniem struktury systemu klasyfikacji Wspólnego Słownika Zamówień, sto-
sując, w zależności od zakresu i rodzaju robót budowlanych objętych zamówieniem, odpowiednio grupy, klasy lub katego-
rie robót określonych we Wspólnym Słowniku Zamówień. 

4. Jeżeli zamówienie na roboty budowlane obejmuje budowę w rozumieniu art. 3 pkt 6 ustawy z dnia 7 lipca 1994 r. – 
Prawo budowlane (Dz. U. z 2021 r. poz. 2351), to składniki kosztów odpowiadają co najmniej grupom robót w rozumieniu 
Wspólnego Słownika Zamówień i obejmują: 

1) 

2) 

koszty robót przygotowania terenu; 

koszty robót budowy obiektów podstawowych; 

2)  Zmiany wymienionego rozporządzenia zostały ogłoszone w Dz. Urz. UE L 329 z 17.12.2003, str. 1 – Dz. Urz. UE Polskie wydanie 
specjalne, rozdz. 6, t. 6, str. 72, Dz. Urz. UE L 235 z 30.08.2006, str. 24, Dz. Urz. UE L 74 z 15.03.2008, str. 1, Dz. Urz. UE L 188 
z 18.07.2009, str. 14 oraz Dz. Urz. UE L 387 z 19.11.2020, str. 29. 

      
 
 
                                                           
Dziennik Ustaw 

– 5 –

Poz. 2458 

3)

4)

5)

koszty robót instalacyjnych;

koszty robót wykończeniowych;

koszty robót związanych z zagospodarowaniem terenu i budową obiektów pomocniczych.

5. Wskaźnik cenowy danego składnika kosztów określa się na podstawie danych rynkowych lub w przypadku braku

takich danych – na podstawie powszechnie stosowanych katalogów i informatorów cenowych. 

6. Liczbę jednostek odniesienia określa się na podstawie programu funkcjonalno-użytkowego.

§ 9. 1.  W przypadku  gdy  brak  jest  odpowiednich  wskaźników  cenowych,  o których  mowa w § 8 ust. 2 pkt 2, koszty

oblicza się w indywidualnym preliminarzu kosztów. 

2. Przy sporządzaniu preliminarza kosztów dopuszcza się korzystanie z dostępnych aktualnych publikacji.

3. Dopuszcza  się  również  sporządzenie  preliminarza  na  podstawie  analizy  kosztów  zrealizowanych  zamówień  bądź

ich części oraz na podstawie analiz indywidualnych. 

4. Źródła informacji przy indywidualnym zbieraniu danych stanowią:

zawarte umowy,

ceny pochodzące z aktualnych publikacji, informatorów, katalogów i ofert lub

dane prognostyczne w zakresie kształtowania się cen. 

1)

2)

3)

Metody i podstawy obliczania planowanych kosztów prac projektowych 

Rozdział 4 

§ 10. 1. Planowane koszty prac projektowych oblicza się jako iloczyn wskaźnika procentowego i planowanych kosz-

tów robót budowlanych, według wzoru: 

WPP = W% × WRB 

w którym poszczególne symbole oznaczają: 

WPP  – planowane koszty prac projektowych; 

W% – wskaźnik procentowy; 

WRB – planowane koszty robót budowlanych. 

2. Podstawę obliczenia planowanych kosztów prac projektowych stanowią:

1)

2)

program funkcjonalno-użytkowy;

planowane koszty robót budowlanych;

3) wskaźniki procentowe.

3. Wskaźnik procentowy przyjmuje się w wysokości i na warunkach określonych w załączniku do rozporządzenia.

4. Planowane koszty prac projektowych stanowią sumę kosztów prac projektowych ustalonych odrębnie dla poszcze-

gólnych obiektów. 

5. Planowane koszty prac projektowych obliczone zgodnie z ust. 1–4 nie obejmują opracowania danych wyjściowych,

a w szczególności: 

1)

2)

3)

4)

5)

uzyskania mapy do celów prawnych, opracowania mapy do celów projektowych;

opracowania dokumentacji geologiczno-inżynierskiej;

opracowania operatów ochrony środowiska;

inwentaryzacji obiektów, zagospodarowania terenu;

inwentaryzacji i waloryzacji zieleni.

Dziennik Ustaw 

– 6 – 

Poz. 2458 

6. Jeżeli  zachodzi  konieczność  ustalenia  udziału  poszczególnych  faz  opracowań  w łącznym  koszcie  prac  projekto-
wych lub ustalenia kosztu opracowań projektowych zlecanych odrębnie, stosuje się następujące wartości procentowe, do-
stosowując udział procentowy do specyfiki inwestycji: 

1) 

2) 

3) 

projekt koncepcyjny – 7–15% wartości prac projektowych, 

projekt budowlany – 30–45% wartości prac projektowych, 

projekt wykonawczy – 40–60% wartości prac projektowych 

– przy czym suma wartości składowych prac projektowych liczona w procentach wynosi 100%. 

7. Jeżeli  opracowanie  ma  pomijać  fazę  projektu  koncepcyjnego,  wartość  udziału  procentowego  faz  następnych  po-

większa się tak, aby łączna ich wartość wynosiła 100%. 

8. W przypadku gdy nie jest możliwe ustalenie wartości wskaźnika procentowego na podstawie załącznika do rozpo-
rządzenia, zamawiający ustala go na podstawie własnych danych lub informacji uzyskanych od właściwej izby samorządu 
zawodowego. 

Rozdział 5 

Przepis przejściowy i przepis końcowy 

§ 11. 1.  Do  postępowań  o udzielenie  zamówienia  wszczętych  i niezakończonych  przed  dniem  1 stycznia  2022 r. 

w zakresie ustalenia wartości zamówienia stosuje się przepisy dotychczasowe. 

2. Zamawiający, w celu ustalenia wartości zamówienia w przypadku przygotowania lub przeprowadzenia postępowa-
nia o udzielenie zamówienia wszczętego po dniu 31 grudnia 2021 r., może korzystać odpowiednio z kosztorysu inwestor-
skiego albo planowanych kosztów prac projektowych oraz planowanych kosztów robót budowlanych określonych w pro-
gramie  funkcjonalno-użytkowym,  sporządzonych  przed  dniem  wejścia  w życie  rozporządzenia,  w przypadku  gdy  od  ich 
sporządzenia upłynęło nie więcej niż 6 miesięcy. 

§ 12. Rozporządzenie wchodzi w życie z dniem 1 stycznia 2022 r.3) 

Minister Rozwoju i Technologii: P. Nowak 

3)  Niniejsze  rozporządzenie  było  poprzedzone  rozporządzeniem  Ministra  Infrastruktury  z dnia  18 maja  2004 r.  w sprawie  określenia 
metod  i podstaw  sporządzania  kosztorysu  inwestorskiego,  obliczania  planowanych  kosztów  prac  projektowych  oraz  planowanych 
kosztów robót budowlanych określonych w programie funkcjonalno-użytkowym (Dz. U. poz. 1389), które traci moc z dniem wej-
ścia  w życie  niniejszego  rozporządzenia  zgodnie  z art. 97  ustawy  z dnia  11 września  2019 r.  –  Przepisy  wprowadzające  ustawę  – 
Prawo zamówień publicznych (Dz. U. poz. 2020 oraz z 2020 r. poz. 1086 i 2275). 

      
 
 
 
 
                                                           
Dziennik Ustaw 

– 7 – 

Poz. 2458 

Załącznik do rozporządzenia Ministra Rozwoju i Technologii 
z dnia 20 grudnia 2021 r. (poz. 2458) 

WYSOKOŚĆ I WARUNKI PRZYJMOWANIA WSKAŹNIKA PROCENTOWEGO W% 

I .   W s k a ź n i k i   p r o c e n t o w e   d l a   o b i e k t ó w   k u b a t u r o w y c h  

1. Wskaźnik procentowy W%, stosowany do obliczenia wartości prac projektowych WPP, określa się według tabeli 1 

w zależności od kategorii złożoności, którą ustala się zgodnie z tabelą 2. 

Tabela 1 

Wartość planowanych 
kosztów robót budowlanych 
w tys. PLN 

do 200 

500 

1000 

2000 

5000 

10 000 

20 000 

50 000 

100 000 

200 000 

500 000 

I 

3,50 

3,25 

3,00 

2,80 

2,60 

2,40 

2,25 

II 

5,00 

4,60 

4,20 

3,90 

3,60 

3,30 

3,00 

2,80 

2,55 

Wskaźnik procentowy W% 

Kategorie złożoności 

III 

IV 

V 

VI 

5,95 

5,45 

5,00 

4,55 

4,20 

3,80 

3,50 

3,20 

2,90 

2,70 

7,55 

6,90 

6,25 

5,90 

5,20 

4,70 

4,30 

3,90 

3,55 

8,65 

7,85 

7,10 

6,45 

5,85 

5,30 

4,80 

4,40 

9,40 

8,50 

7,70 

7,00 

6,30 

5,70 

5,20 

2. Podane w tabeli 1 wartości W% odnoszą się do projektowania nowych obiektów kubaturowych. W przypadku remontu, 
rozbudowy, nadbudowy czy przebudowy wartość W% powiększa się o 15–30%, w zależności od stopnia skomplikowania 
projektowanych robót. W przypadku rozbudowy poziomej, niewymagającej ingerencji w układ funkcjonalny, konstrukcję 
lub instalacje obiektu istniejącego, wartość W% powiększa się o 5–15%, w zależności od stopnia skomplikowania projek-
towanych robót. 

3. Dla  określenia  wartości  W%  obiektów,  których  planowane  koszty  robót  budowlanych  wyrażają się wielkościami 

pośrednimi w stosunku do zawartych w tabeli, stosuje się interpolację liniową. 

4. Obiekty kubaturowe zalicza się do następujących kategorii złożoności: 

1) 

2) 

3) 

4) 

5) 

6) 

kategoria  I –  najprostsze  budynki  otwarte  lub  półotwarte,  wiaty  oraz  jednoprzestrzenne  niepodpiwniczone  budynki 
parterowe, bez wyposażenia instalacyjnego (z wyjątkiem najprostszych instalacji elektrycznych i wentylacji grawita-
cyjnej) i technologicznego, nieprzeznaczone na pobyt ludzi; 

kategoria II – proste budynki jednokondygnacyjne bez podpiwniczenia i z podpiwniczeniem, wyposażone w najprost-
sze instalacje (wodno-kanalizacyjne, centralnego ogrzewania, instalacje elektryczne, wentylację grawitacyjną), z naj-
prostszym wyposażeniem technologicznym; 

kategoria III – budynki niskie o małym stopniu trudności, o prostej jednorodnej funkcji, z podstawowym wyposaże-
niem instalacyjnym i technologicznym; 

kategoria IV – budynki o złożonych wymaganiach funkcjonalnych, instalacyjnych i technologicznych o średnim stop-
niu trudności, niezaliczone do kategorii V i VI; 

kategoria V – budynki wielofunkcyjne oraz o bardzo złożonych wymaganiach funkcjonalnych, instalacyjnych i tech-
nologicznych, wymagające szczególnych rozwiązań inżynierskich, budynki wysokościowe; 

kategoria VI – budynki o najwyższym stopniu skomplikowania funkcjonalnego, instalacyjnego i technologicznego, z wbu-
dowanymi złożonymi konstrukcjami inżynierskimi, unikalnymi instalacjami i wyposażeniem, budynki o najwyższych 
wymaganiach co do standardu wykończenia i prestiżu. 

      
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
Dziennik Ustaw 

Tabela 2 

Objaśnienia do tabeli: 

– 8 – 

Poz. 2458 

•  –  klasyfikacja do kategorii w przypadku podstawowych wymagań stawianych danemu obiektowi (budynkowi) zgodnie 

z opisem kategorii – ust. 2; 

▄  –  klasyfikacja  do  kategorii  wyższej  w przypadku  wymagań  większych  niż  podstawowe,  jeżeli  projektowany  obiekt 

łączy w sobie więcej niż jedną z funkcji opisanych w tabeli. 

Grupy 
funkcjonalne 

Funkcje obiektów 

Kategorie złożoności 

I 

II 

III 

IV 

V 

VI 

1. Mieszkalne 

1) 

2) 

3) 

4) 

5) 

6) 

7) 

8) 

9) 

1) 

2) 

3) 

4) 

5) 

proste jednokondygnacyjne budynki 
mieszkalne jednorodzinne 
(niepodpiwniczone, bez garaży), proste 
domy letniskowe itp. 

budynki mieszkalne jednorodzinne 
z garażami, budynki mieszkalne 
jednorodzinne w zabudowie bliźniaczej, 
domy wakacyjne (całoroczne) itp. 

budynki mieszkalne jednorodzinne 
z indywidualnymi wymaganiami, budynki 
mieszkalne jednorodzinne tarasowe itp. 

rezydencje o najwyższym standardzie 

budynki mieszkalne jednorodzinne 
w zabudowie zwartej (szeregowej, 
łańcuchowej, dywanowej) 

budynki mieszkalne wielorodzinne niskie 
(do 12 m), bez garaży i wind 

budynki mieszkalne wielorodzinne (poza 
wyżej i niżej wymienionymi) 

budynki mieszkalne wielorodzinne 
wysokościowe (ponad 55 m) 

budynki mieszkalne wielorodzinne 
o najwyższym standardzie 
z indywidualnymi wnętrzami pod klucz 

proste obiekty biura, jednokondygnacyjne, 
bez garaży 

• 

2. Biura 

budynki biurowe wielokondygnacyjne 
niskie (do 12 m), bez garaży 

budynki biurowe wielokondygnacyjne 
średnio wysokie i wysokie (do 55 m), 
z garażami (piętra typu open space, bez 
aranżacji) 

jw. z częścią żywieniową (stołówką) oraz 
zespołem odnowy biologicznej 

budynki biurowe wielokondygnacyjne 
wysokościowe (powyżej 55 m), z garażami 
(piętra typu open space, bez aranżacji) 

• 

▄ 

• 

▄ 

▄ 

• 

• 

▄ 

• 

▄ 

• 

• 

▄ 

• 

▄ 

▄ 

▄ 

• 

▄ 

• 

• 

▄ 

• 

▄ 

▄ 

• 

▄ 

▄ 

      
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
Dziennik Ustaw 

– 9 – 

Poz. 2458 

6) 

7) 

1) 

2) 

3) 

4) 

5) 

6) 

7) 

1) 

2) 

3) 

1) 

2) 

3) 

4) 

1) 

2) 

jw. z częścią żywieniową (stołówką) oraz 
zespołem odnowy biologicznej, usługami 

budynki biurowe ze specjalnymi 
wymaganiami (np. prestiżowe siedziby 
instytucji lub firm) 

siedziby gmin, powiatów, starostw 

• 

3. Administracja i łączność 

siedziby władz wojewódzkich, ratusze 

siedziby administracji rządowej 

siedziby administracji rządowej 
o najwyższym standardzie, ambasady 

sądy rejonowe i okręgowe 

sądy apelacyjne 

placówki pocztowe (bez części 
telekomunikacyjnej), centrale telefoniczne 

4. Straż, policja, więzienia 

• 

▄ 

▄ 

• 

• 

• 

remizy strażackie, komisariaty, więzienia 

siedziby straży z pełnym zapleczem 
technicznym, siedziby komend wojewódzkich 
Policji i Komendy Stołecznej Policji 

posterunki Policji, areszty 

5. Handel i usługi 

proste, jednofunkcyjne pawilony handlowe 
lub usługowe, obiekty małej gastronomii 

supermarkety bez uzupełniającego programu 
wydzielonych sklepów i usług, hale 
targowe, kantyny, stołówki, jadłodajnie 

supermarkety z uzupełniającym programem 
wydzielonych sklepów i usług, domy 
towarowe, obiekty (pawilony) handlowe 
i handlowo-usługowe o różnorodnym 
programie, salony samochodowe, sklepy 
specjalistyczne, kawiarnie, herbaciarnie, 
bary, puby 

budynki centrów handlowych z wieloma 
placówkami handlowymi, usługowymi, 
gastronomicznymi oraz zapleczem (także 
galerie typu shopping mall), sklepy 
specjalistyczne ze specjalnymi 
wymaganiami co do wykończenia 
i ekspozycji, restauracje 

• 

• 

▄ 

▄ 

• 

▄ 

▄ 

▄ 

• 

• 

• 

▄ 

• 

▄ 

▄ 

• 

▄ 

• 

▄ 

• 

▄ 

▄ 

▄ 

▄ 

• 

▄ 

6. Oświata 

przedszkola, internaty bez części 
żywieniowej i sportowej 

szkoły podstawowe z częścią żywieniową 
lub z zapleczem sportowym, internaty 
z częścią żywieniową lub sportową 

• 

▄ 

• 

▄ 

      
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
Dziennik Ustaw 

– 10 – 

Poz. 2458 

3) 

1) 

2) 

3) 

4) 

1) 

2) 

3) 

4) 

5) 

1) 

2) 

3) 

4) 

5) 

1) 

2) 

licea, szkoły zawodowe (z warsztatami lub 
laboratoriami) 

• 

▄ 

7. Nauka 

▄ 

• 

▄ 

• 

▄ 

• 

▄ 

• 

▄ 

▄ 

• 

▄ 

• 

▄ 

• 

▄ 

• 

▄ 

• 

• 

▄ 

• 

▄ 

• 

hotele asystenckie, akademiki (bez zaplecza 
żywieniowego i rekreacyjnego) 

obiekty wydziałów wyższych uczelni bez 
laboratoriów, administracja wyższych 
uczelni, akademiki z zapleczem 
żywieniowym i rekreacyjnym 

obiekty wydziałów wyższych uczelni 
z laboratoriami, instytuty naukowe  
(z laboratoriami) 

laboratoria naukowe 

8. Zdrowie i opieka społeczna 

• 

• 

noclegownie 

żłobki, domy dziecka 

przychodnie zdrowia, domy spokojnej 
starości 

budynki centrów medycznych z zapleczem 
diagnostycznym, hospicja, budynki 
ośrodków rehabilitacyjnych, domy 
uzdrowiskowe z zapleczem 
rehabilitacyjnym 

oddziały zabiegowe szpitali, kliniki 

9. Kultura 

świetlice 

kluby mieszkańców, dyskoteki 

kina (1 albo 2 sale dla ok. 200 osób lub 
mniejsze), sale i pawilony wystawowe, 
galerie, małe muzea – pawilony muzealne, 
biblioteki bez zaplecza konserwacji 
zbiorów, magazynów zwartych  
(w szczególności małe miejskie lub gminne – 
ok. 100 000 woluminów i mniejsze) 

multikina, teatry o ograniczonej technologii, 
cyrki stałe, sale wielofunkcyjne, sale 
kongresowe, biblioteki o pełnym programie 

teatry, opery, operetki, filharmonie, sale 
koncertowe, muzea o najwyższym 
standardzie, centra kongresowe 

10. Sport i rekreacja 

wiaty (w tym na sprzęt pływający), 
magazyny sprzętu sportowego, hangary na 
łodzie 

domki campingowe letnie (bez łazienek), 
pawilony, szatnie, umywalnie i toalety, 
przebieralnie 

• 

▄ 

• 

▄ 

      
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
Dziennik Ustaw 

– 11 – 

Poz. 2458 

3) 

4) 

5) 

1) 

2) 

3) 

1) 

2) 

3) 

1) 

2) 

3) 

4) 

5) 

6) 

1) 

2) 

3) 

4) 

domki wypoczynkowe (z toaletami 
i łazienkami) 

sale gimnastyczne i sportowe dla gier 
zespołowych (koszykówka, siatkówka, piłka 
ręczna itp., zespoły odnowy biologicznej 
(basen, jacuzzi, sauna, masaż itp.) 

pływalnie kryte, lodowiska kryte, 
wielofunkcyjne budynki sportowe 

11. Hotele i turystyka 

schroniska, hotele młodzieżowe, hotele 
turystyczne, hotele pracownicze, 
asystenckie, motele, hotele niższych 
kategorii (nie więcej niż dwugwiazdkowe), 
pensjonaty 

hotele trzygwiazdkowe, domy wczasowe 
(pełne zaplecze rekreacyjne, żywieniowe) 

hotele czterogwiazdkowe 
i pięciogwiazdkowe 

▄ 

• 

▄ 

• 

proste budynki gospodarcze, szopy, stodoły  • 

12. Rolnicze 

stajnie, chlewnie, obory, budynki 
inwentarskie, szklarnie 

kliniki weterynaryjne 

13. Komunikacja 

jednokondygnacyjne garaże wolnostojące 
i boksowe, wielostanowiskowe garaże 
jednokondygnacyjne nadziemne (bez ścian) 

• 

wielostanowiskowe garaże nadziemne 
zamknięte, garaże podziemne 

stacje obsługi pojazdów, stacje paliw 

stacje paliw z usługami (sklep, kawiarnia, 
restauracja), budynki małych dworców 
autobusowych i stacji kolejowych 

budynki dworców kolejowych 

budynki pasażerskich dworców lotniczych 

14. Przemysł i magazyny 

▄ 

• 

wiaty, najprostsze budynki magazynowe, 
portiernie 

• 

magazyny, proste budynki warsztatowe, 
zajezdnie środków transportu, myjnie itp. 

proste budynki produkcyjne, magazyny 
zmechanizowane, stacje sprężarek itp. 

hale produkcyjne z zapleczem techniczno-
-magazynowym i socjalnym, kotłownie 
wysokoparametrowe, ujęcia wody, proste 
oczyszczalnie ścieków itp. 

• 

▄ 

• 

▄ 

• 

▄ 

• 

▄ 

▄ 

• 

▄ 

• 

▄ 

▄ 

• 

▄ 

• 

▄ 

• 

▄ 

▄ 

▄ 

• 

▄ 

• 

▄ 

• 

▄ 

▄ 

• 

      
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
Dziennik Ustaw 

– 12 – 

Poz. 2458 

5) 

6) 

1) 

2) 

3) 

złożone hale produkcyjne, wytwórnie farb, 
drukarnie, tłocznie, budynki utylizacji 
odpadów, duże oczyszczalnie ścieków, 
budynki zagrożone wybuchem, 
promieniowaniem jonizującymi itp. 

unikalne budynki przemysłowe, techniki 
jądrowej, clean roomy itp. o najwyższych 
wymaganiach technologicznych, 
instalacyjnych, konstrukcyjnych 
i wykończeniowych 

• 

▄ 

• 

15. Militarne i obrony cywilnej 

koszary bez części żywieniowej i sportowej, 
schrony z prostym wyposażeniem 
technologicznym 

koszary z częścią żywieniową lub sportową, 
schrony ze złożonym wyposażeniem 
technologicznym 

stanowiska dowodzenia ze złożonym 
wyposażeniem technologicznym 

• 

▄ 

• 

▄ 

• 

▄ 

I I .   W s k a ź n i k i   p r o c e n t o w e   d l a   o b i e k t ó w   l i n i o w y c h  

A. Wskaźniki dla budownictwa kolejowego 

Obiekty budownictwa kolejowego 

Wskaźnik procentowy W% 

Lp. 

1 

2 

Stacje kolejowe, przystanek autobusowy 

Szlak kolejowy 

3  Urządzenia SRK (sterowanie ruchem kolejowym) 

4 

Sieć trakcyjna 

5  Linia potrzeb nietrakcyjnych LPN 

5,0–6,0% 

6,0–8,0% 

6,0–8,0% 

6,0% 

3,5% 

B. Wskaźniki dla budownictwa drogowego 

Lp. 

Inwestycje drogowe 

Wskaźnik procentowy W% 

1  Autostrady, drogi ekspresowe 

2  Drogi klasy GP 

3  Drogi klasy G i niższych klas 

4  Ulice bez względu na klasę 

3,0–5,0% 

2,5–4,5% 

2,5–4,0% 

2,5–5,0% 

C. Wskaźniki dla sieci: ciepłowniczych, wodociągowych, kanalizacyjnych, gazowych (niskoparametrowych) 
oraz sieci elektroenergetycznych niskiego i średniego napięcia 

Lp. 

1 

Wyszczególnienie 

Wskaźnik procentowy W% 

Sieci ciepłownicze 
Koszty inwestycji 0,5–1,0 mln 
Koszty inwestycji 1,0–3,0 mln 
Koszty inwestycji 3,0–5,0 mln 
Koszty inwestycji powyżej 5,0 mln 

5,0–7,0% 
3,5–5,5% 
3,0–4,5% 
2,5–3,5% 

      
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
Dziennik Ustaw 

– 13 – 

Poz. 2458 

2 

3 

4 

Węzły cieplne 
Koszty inwestycji 0,1–0,2 mln 
Koszty inwestycji 0,2–0,5 mln 
Koszty inwestycji powyżej 0,5 mln 

Sieci wodociągowe 
Koszty inwestycji 0,5–1,0 mln 
Koszty inwestycji 1,0–3,0 mln 
Koszty inwestycji 3,0–5,0 mln 
Koszty inwestycji powyżej 5,0 mln 

Sieci kanalizacyjne 
Koszty inwestycji 0,5–1,0 mln 
Koszty inwestycji 1,0–3,0 mln 
Koszty inwestycji 3,0–5,0 mln 
Koszty inwestycji powyżej 5,0 mln 

Sieci gazowe niskoparametrowe 
Koszty inwestycji 0,5–1,0 mln 
Koszty inwestycji 1,0–3,0 mln 
Koszty inwestycji 3,0–5,0 mln 
Koszty inwestycji powyżej 5,0 mln 

5 

Sieci elektroenergetyczne niskiego i średniego napięcia 

7,0–9,0% 
5,0–7,0% 
3,0–5,0% 

5,5–7,5% 
4,5–6,5% 
4,0–5,5% 
3,5–4,5% 

6,0–8,0% 
5,5–7,5% 
5,0–6,5% 
4,0–6,0% 

5,0–7,0% 
4,5–6,5% 
4,0–5,5% 
3,5–4,5% 

6,0–14,0% 

I I I .   W s k a ź n i k i   p r o c e n t o w e   d l a   o b i e k t ó w   i n ż y n i e r s k i c h  

Lp. 

Kategoria złożoności, przykładowe obiekty 

Wskaźnik procentowy W% 

1  Kategoria I: proste obiekty inżynierskie, np.: 

3,0–4,5% 

• mosty belkowe i płytowe, statycznie wyznaczalne, 
• mosty drewniane, 
• przejścia wielopoziomowe (dla pieszych), 
• konstrukcje w budownictwie energetycznym, 
• zapory ziemne, 
• maszty i konstrukcje wieżowe do 100 m, 
• wielkoprzestrzenne obiekty budownictwa ziemnego, w tym dla górnictwa 
odkrywkowego 

2  Kategoria II: obiekty o złożonych konstrukcjach i metodach budowy, 

4,5–6,0% 

w szczególności: 
• mosty belkowe i płytowe statycznie niewyznaczalne, 
• kratownicowe i ramowe, 
• wiadukty, tunele, 
• chłodnie kominowe, 
• zapory betonowe, 
• kominy o wysokości ponad 200 m, 
• maszty i konstrukcje o wys. 101–300 m, 
• oczyszczalnie ścieków, 
• zakłady przeróbki odpadów, 
• zakłady uzdatniania wody 

3  Kategoria III: obiekty o specjalnych konstrukcjach i metodach budowy, 

5,5–7,5% 

w szczególności: 
• mosty łukowe, wiszące i podwieszone, 
• wielopoziomowe wiadukty i węzły komunikacyjne, 
• wiadukty o znacznej wysokości (h ≥ 8 m) 

      
 
 
 
 
 
 
 
Warszawa, dnia 29 grudnia 2021 r.

Poz. 2463

ROZPORZĄDZENIE
MINISTRA ROZWOJU I TECHNOLOGII 1) 

z dnia 20 grudnia 2021 r.

w sprawie zakresu informacji zawartych w rocznym sprawozdaniu o udzielonych zamówieniach, jego wzoru, 
sposobu przekazywania oraz sposobu i trybu jego korygowania2)

Na podstawie art. 82 ust. 4 ustawy z dnia 11 września 2019 r. – Prawo zamówień publicznych (Dz. U. z 2021 r. poz. 1129, 

1598, 2054 i 2269) zarządza się, co następuje:

§ 1. 1. Roczne sprawozdanie o udzielonych zamówieniach, zwane dalej „sprawozdaniem”, składane przez zamawiają-

cego zawiera:

1) 

dane dotyczące zamawiającego obejmujące:

a)  nazwę  zamawiającego,  informacje  o  oddziale  zamawiającego,  adres  siedziby  zamawiającego,  krajowy  numer 
identyfikacyjny, adres strony internetowej zamawiającego, adres poczty elektronicznej oraz numer telefonu,

b)  oznaczenie rodzaju zamawiającego, a w przypadku zamawiających sektorowych, o których mowa w art. 5 ust. 1 
ustawy z dnia 11 września 2019 r. – Prawo zamówień publicznych, zwanej dalej ,,ustawą”, oznaczenie rodzaju 
wykonywanej działalności sektorowej, o której mowa w art. 5 ust. 4 ustawy;

2) 

oznaczenie czynności zamawiającego, polegającej na złożeniu sprawozdania albo na korekcie sprawozdania;

3)  w przypadku zamówień klasycznych o wartości mniejszej niż progi unijne, o których mowa w dziale III ustawy, z wy-
łączeniem zamówień na usługi społeczne i inne szczególne usługi – liczbę postępowań zakończonych udzieleniem 
zamówienia albo zawarciem umowy ramowej i wartość zawartych umów, bez podatku od towarów i usług, z podzia-
łem na roboty budowlane, dostawy i usługi oraz na tryby udzielania zamówienia albo zawarcia umowy ramowej;

4)  w przypadku zamówień o wartości równej lub przekraczającej progi unijne, z wyłączeniem zamówień na usługi spo-
łeczne i inne szczególne usługi – wykaz postępowań zakończonych udzieleniem zamówienia ze wskazaniem: 

a)  numeru ogłoszenia o udzieleniu zamówienia w Dzienniku Urzędowym Unii Europejskiej, 

1)  Minister Rozwoju i Technologii kieruje działem administracji rządowej – gospodarka, na podstawie § 1 ust. 2 pkt 2 rozporządzenia 
Prezesa Rady Ministrów z dnia 27 października 2021 r. w sprawie szczegółowego zakresu działania Ministra Rozwoju i Technologii 
(Dz. U. poz. 1945).

2)  Niniejsze rozporządzenie w zakresie swojej regulacji wdraża: 

1) 

2) 

3) 

 dyrektywę Parlamentu Europejskiego i Rady 2014/24/UE z dnia 26 lutego 2014 r. w sprawie zamówień publicznych, uchylającą 
dyrektywę 2004/18/WE (Dz. Urz. UE L 94 z 28.03.2014, str. 65, Dz. Urz. UE L 307 z 25.11.2015, str. 5, Dz. Urz. UE L 24 
z 30.01.2016, str. 14, Dz. Urz. UE L 337 z 19.12.2017, str. 19, Dz. Urz. UE L 279 z 31.10.2019, str. 25 oraz Dz. Urz. UE L 398 
z 11.11.2021, str. 23); 
 dyrektywę Parlamentu Europejskiego i Rady 2014/25/UE z dnia 26 lutego 2014 r. w sprawie udzielania zamówień przez pod-
mioty działające w sektorach gospodarki wodnej, energetyki, transportu i usług pocztowych, uchylającą dyrektywę 2004/17/WE 
(Dz. Urz. UE L 94 z 28.03.2014, str. 243, Dz. Urz. UE L 307 z 25.11.2015, str. 7, Dz. Urz. UE L 337 z 19.12.2017, str. 17,  
Dz. Urz. UE L 279 z 31.10.2019, str. 27 oraz Dz. Urz. UE L 398 z 11.11.2021, str. 25); 
 dyrektywę Parlamentu Europejskiego i Rady 2009/81/WE z dnia 13 lipca 2009 r. w sprawie koordynacji procedur udzielania 
niektórych zamówień na roboty budowlane, dostawy i usługi przez instytucje lub podmioty zamawiające w dziedzinach obron-
ności i bezpieczeństwa i zmieniającą dyrektywy 2004/17/WE i 2004/18/WE (Dz. Urz. UE L 216 z 20.08.2009, str. 76, Dz. Urz. 
UE L 314 z 01.12.2009, str. 64, Dz. Urz. UE L 319 z 02.12.2011, str. 43, Dz. Urz. UE L 158 z 10.06.2013, str. 184, Dz. Urz.  
UE L 335 z 14.12.2013, str. 17, Dz. Urz. UE L 330 z 16.12.2015, str. 14, Dz. Urz. UE L 337 z 19.12.2017, str. 22, Dz. Urz.  
UE L 198 z 25.07.2019, str. 241, Dz. Urz. UE L 279 z 31.10.2019, str. 29 oraz Dz. Urz. UE L 398 z 11.11.2021, str. 19).

DZIENNIK USTAWRZECZYPOSPOLITEJ  POLSKIEJ 
 
 
Dziennik Ustaw 

– 2 –  

Poz. 2463

b) 

rodzaju zamówienia, 

c)  przedmiotu zamówienia ze wskazaniem kodów określonych w rozporządzeniu (WE) nr 2195/2002 Parlamentu 
Europejskiego i Rady z dnia 5 listopada 2002 r. w sprawie Wspólnego Słownika Zamówień (CPV) (Dz. Urz.  
WE  L 340 z 16.12.2002, str. 1, z późn.  zm.3) − Dz. Urz. UE Polskie wydanie specjalne, rozdz.  6, t. 5, str. 3, 
z późn. zm.), zwanym dalej ,,Wspólnym Słownikiem Zamówień”, 

d) 

trybu udzielenia zamówienia albo zawarcia umowy ramowej,

e)  kraju pochodzenia wybranego wykonawcy lub wykonawców, oznaczającego kraj, w którym wybrany wykonaw-

ca lub wykonawcy mają siedzibę lub miejsce zamieszkania, 

f)  wartości zawartych umów bez podatku od towarów i usług, 

g) 

liczby części, w których udzielono zamówienia, jeżeli w ramach jednego postępowania zamawiający dopuścił 
składanie ofert częściowych,

h) 

liczby złożonych ofert, 

i) 

j) 

k) 

liczby odrzuconych ofert, 

liczby ofert złożonych przez wykonawców z sektora mikro-, małych i średnich przedsiębiorców w rozumieniu 
ustawy z dnia 6 marca 2018 r. – Prawo przedsiębiorców (Dz. U. z 2021 r. poz. 162 i 2105), 

czy zamówienia udzielono wykonawcy z sektora mikro-, małych i średnich przedsiębiorców w rozumieniu usta-
wy z dnia 6 marca 2018 r. – Prawo przedsiębiorców

–  w  odniesieniu  i  z  podziałem  na  zamówienia  klasyczne,  zamówienia  sektorowe  oraz  zamówienia  w  dziedzinach 
obronności i bezpieczeństwa;

5)  w przypadku postępowań o udzielenie zamówienia, do których stosuje się przepisy ustawy, w których wystąpił konflikt 
interesów, o którym mowa w art. 56 ust. 2 ustawy, w tym skutkujący wykluczeniem wykonawcy na podstawie art. 109 
ust. 1 pkt 6 ustawy, lub w których wykonawcy zawarli porozumienie mające na celu zakłócenie konkurencji, o którym 
mowa w art. 108 ust. 1 pkt 5 ustawy – wykaz postępowań zakończonych udzieleniem zamówienia ze wskazaniem: 

a)  numeru ogłoszenia o wyniku postępowania w Biuletynie Zamówień Publicznych albo ogłoszenia o udzieleniu 

zamówienia w Dzienniku Urzędowym Unii Europejskiej, 

b) 

rodzaju zamówienia, 

c)  przedmiotu zamówienia ze wskazaniem kodów określonych we Wspólnym Słowniku Zamówień, 

d) 

e) 

f) 

trybu udzielenia zamówienia albo zawarcia umowy ramowej, 

liczby przypadków wystąpienia konfliktu interesów, o którym mowa w art. 56 ust. 2 ustawy, w tym skutkującego 
wykluczeniem wykonawcy na podstawie art. 109 ust. 1 pkt 6 ustawy,

liczby porozumień mających na celu zakłócenie konkurencji, o których mowa w art. 108 ust. 1 pkt 5 ustawy, 
z podziałem na liczbę wykrytych porozumień mających na celu zakłócenie konkurencji oraz liczbę wykonaw-
ców, których oferta została odrzucona, jako podlegających wykluczeniu z udziału w postępowaniu o udzielenie 
zamówienia na podstawie art. 108 ust. 1 pkt 5 ustawy

–  w  odniesieniu  i  z  podziałem  na  zamówienia  klasyczne,  zamówienia  sektorowe  oraz  zamówienia  w  dziedzinach 
obronności i bezpieczeństwa;

6)  w przypadku zamówień na usługi społeczne i inne szczególne usługi, o których mowa w art. 359 i art. 392 ust. 1 usta-

wy – wykaz postępowań zakończonych udzieleniem zamówienia ze wskazaniem: 

a)  numeru ogłoszenia o wyniku postępowania w Biuletynie Zamówień Publicznych albo ogłoszenia o udzieleniu 

zamówienia w Dzienniku Urzędowym Unii Europejskiej,

b)  opisu usługi, 

c)  przedmiotu zamówienia ze wskazaniem kodów określonych we Wspólnym Słowniku Zamówień,

d) 

e) 

trybu udzielenia zamówienia albo zawarcia umowy ramowej, 

czy zamówienia udzielono jako zamówienie zastrzeżone na usługi zdrowotne, społeczne oraz kulturalne, o któ-
rym mowa w art. 361 ust. 1 lub art. 392 ust. 3 ustawy,

3)  Zmiany wymienionego rozporządzenia zostały ogłoszone w Dz. Urz. UE L 329 z 17.12.2003, str. 1 – Dz. Urz. UE Polskie wydanie 
specjalne, rozdz. 6, t. 6, str. 72, Dz. Urz. UE L 235 z 30.08.2006, str. 24, Dz. Urz. UE L 74 z 15.03.2008, str. 1, Dz. Urz. UE L 188 
z 18.07.2009, str. 14 oraz Dz. Urz. UE L 387 z 19.11.2020, str. 29.

 
Dziennik Ustaw 

– 3 –  

Poz. 2463

f)  wartości zawartej umowy bez podatku od towarów i usług,

g) 

liczby części, w których udzielono zamówienia, jeżeli w ramach jednego postępowania zamawiający dopuścił 
składanie ofert częściowych,

h) 

liczby złożonych ofert,

i) 

liczby ofert złożonych przez wykonawców z sektora mikro-, małych i średnich przedsiębiorców w rozumieniu 
ustawy z dnia 6 marca 2018 r. – Prawo przedsiębiorców

– w odniesieniu i z podziałem na zamówienia klasyczne oraz zamówienia sektorowe;

7)  w przypadku zamówień, do których stosuje się przepisy ustawy, uwzględniających aspekty społeczne – wykaz postę-

powań zakończonych udzieleniem zamówienia ze wskazaniem: 

a)  numeru ogłoszenia o wyniku postępowania w Biuletynie Zamówień Publicznych albo ogłoszenia o udzieleniu 

zamówienia w Dzienniku Urzędowym Unii Europejskiej, 

b) 

rodzaju zamówienia, 

c)  przedmiotu zamówienia ze wskazaniem kodów określonych we Wspólnym Słowniku Zamówień, 

d) 

trybu udzielenia zamówienia albo zawarcia umowy ramowej, 

e)  wartości zawartej umowy bez podatku od towarów i usług,

f) 

g) 

h) 

i) 

j) 

k) 

liczby części, w których udzielono zamówienia, jeżeli w ramach jednego postępowania zamawiający dopuścił 
składanie ofert częściowych,

liczby złożonych ofert,

czy zamówienia udzielono jako zamówienie zastrzeżone, o którym mowa w art. 94 ust. 1 ustawy,

czy w opisie przedmiotu zamówienia określono wymagania w zakresie dostępności dla osób niepełnosprawnych 
oraz projektowania z przeznaczeniem dla wszystkich użytkowników, o których mowa w art. 100 ust. 1 ustawy, 

czy w kryteriach oceny ofert określono aspekty społeczne, o których mowa w art. 242 ust. 2 ustawy, w tym inte-
grację zawodową i społeczną osób, o których mowa w art. 94 ust. 1 ustawy,

czy w ogłoszeniu o zamówieniu lub dokumentach zamówienia określono wymagania związane z realizacją za-
mówienia, w tym: wymagania w zakresie zatrudnienia na podstawie stosunku pracy, o których mowa w art. 95 
ust. 1 ustawy – w przypadku zamówień na usługi lub roboty budowlane, wymagania obejmujące aspekty społecz-
ne związane z zatrudnieniem, o których mowa w art. 96 ust. 1 ustawy,

l) 

czy określono etykietę związaną z aspektami społecznymi albo inny przedmiotowy środek dowodowy, o których 
mowa w art. 104 ustawy,

m)  rodzaju podmiotu, któremu udzielono zamówienia

–  w  odniesieniu  i  z  podziałem  na  zamówienia  klasyczne,  zamówienia  sektorowe  oraz  zamówienia  w  dziedzinach 
obronności i bezpieczeństwa; 

8)  w przypadku zamówień, do których stosuje się przepisy ustawy, uwzględniających aspekty środowiskowe lub inno-

wacyjne – wykaz postępowań zakończonych udzieleniem zamówienia ze wskazaniem: 

a)  numeru ogłoszenia o wyniku postępowania w Biuletynie Zamówień Publicznych albo ogłoszenia o udzieleniu 

zamówienia w Dzienniku Urzędowym Unii Europejskiej, 

b) 

rodzaju zamówienia, 

c)  przedmiotu zamówienia ze wskazaniem kodów określonych we Wspólnym Słowniku Zamówień, 

d) 

trybu udzielenia zamówienia albo zawarcia umowy ramowej, 

e)  wartości zawartej umowy bez podatku od towarów i usług, 

f) 

g) 

czy w opisie przedmiotu zamówienia określono wymagania środowiskowe zgodnie z art. 101 ustawy, 

czy, w odniesieniu do zdolności technicznej lub zawodowej wykonawców, określono wymóg spełniania wyma-
gań odpowiednich systemów lub norm zarządzania środowiskowego zgodnie z art. 116 ust. 1 ustawy,

h) 

czy w kryteriach oceny ofert określono aspekty środowiskowe: 

–  przez odwołanie się do aspektów środowiskowych, w tym efektywności energetycznej przedmiotu zamówie-

nia, o których mowa w art. 242 ust. 2 ustawy,

–  przez zastosowanie kryterium kosztu z wykorzystaniem rachunku kosztów cyklu życia, o którym mowa w art. 245 

ust. 1 ustawy,

 
Dziennik Ustaw 

– 4 –  

Poz. 2463

i) 

j) 

czy w ogłoszeniu o zamówieniu lub dokumentach zamówienia określono wymagania związane z realizacją za-
mówienia obejmujące aspekty środowiskowe, o których mowa w art. 96 ust. 1 ustawy, 

czy  określono  etykietę  związaną  z  aspektami  środowiskowymi  albo  inny  przedmiotowy  środek  dowodowy, 
o których mowa w art. 104 ustawy, 

k) 

czy w kryteriach oceny ofert określono aspekty innowacyjne, o których mowa w art. 242 ust. 2 ustawy,

l) 

czy w ogłoszeniu o zamówieniu lub dokumentach zamówienia określono wymagania związane z realizacją za-
mówienia obejmujące aspekty związane z innowacyjnością, o których mowa w art. 96 ust. 1 ustawy

–  w  odniesieniu  i  z  podziałem  na  zamówienia  klasyczne,  zamówienia  sektorowe  oraz  zamówienia  w  dziedzinach 
obronności i bezpieczeństwa;

9) 

liczbę i łączną wartość zamówień, bez podatku od towarów i usług, które zostały udzielone z wyłączeniem stosowania 
przepisów ustawy, na podstawie art. 9, art. 10 ust. 1 pkt 3 i 4 oraz ust. 2 pkt 3 i 4, art. 11 ust. 1, ust. 2 pkt 2 i 3, ust. 4 
i ust. 5 pkt 1−9, art. 12 ust. 1, art. 13 ust. 1 pkt 2−8, art. 14, art. 363 ust. 1 i 2, art. 364, art. 365 ust. 1–3 i art. 366 ust. 1 
ustawy;

10)  w przypadku zamówień klasycznych, których wartość jest mniejsza niż 130 000 złotych – łączną wartość udzielonych 

zamówień, bez podatku od towarów i usług;

11)  w przypadku zamówień sektorowych, których wartość jest mniejsza niż progi unijne – łączną wartość udzielonych 

zamówień, bez podatku od towarów i usług, z podziałem na roboty budowlane, dostawy i usługi;

12)  w przypadku zamówień w dziedzinach obronności i bezpieczeństwa, których wartość jest mniejsza niż progi unijne –
łączną wartość udzielonych zamówień, bez podatku od towarów i usług, z podziałem na roboty budowlane, dostawy 
i usługi.

2. Wzór sprawozdania stanowi załącznik do rozporządzenia.

§ 2. Zamawiający przekazuje sprawozdanie Prezesowi Urzędu Zamówień Publicznych przy użyciu formularza umiesz-
czonego i udostępnionego na stronach portalu internetowego Urzędu Zamówień Publicznych, zgodnego z wzorem sprawo-
zdania, o którym mowa w § 1 ust. 2.

§ 3. 1. Zamawiający, niezwłocznie po stwierdzeniu, że informacje zawarte w sprawozdaniu są nieaktualne lub niepo-
prawne, dokonuje korekty sprawozdania i przekazuje ją Prezesowi Urzędu Zamówień Publicznych przy użyciu formularza, 
o którym mowa w § 2.

2. Korekta sprawozdania polega na zmianie, dodaniu lub usunięciu danych w sekcji sprawozdania, w której zamawiają-

cy stwierdził, że zawiera ona nieaktualne lub niepoprawne dane. 

§ 4. 1. W przypadku postępowań o udzielenie zamówienia oraz postępowań o zawarcie umowy ramowej wszczętych 
i niezakończonych przed dniem 1 stycznia 2021 r. do sporządzenia rocznego sprawozdania o udzielonych zamówieniach 
stosuje się przepisy dotychczasowe.

2.  W  przypadku  postępowań  o  udzielenie  zamówienia  oraz  postępowań  o  zawarcie  umowy  ramowej  wszczętych  po 

dniu 31 grudnia 2020 r., lecz przed dniem wejścia w życie niniejszego rozporządzenia, które:

1) 

2) 

zostały zakończone przed dniem wejścia w życie niniejszego rozporządzenia,

nie zostały zakończone przed dniem wejścia w życie niniejszego rozporządzenia

– do sporządzenia rocznego sprawozdania o udzielonych zamówieniach stosuje się przepisy niniejszego rozporządzenia.

§ 5. Rozporządzenie wchodzi w życie z dniem 1 stycznia 2022 r.4)

Minister Rozwoju i Technologii: P. Nowak

4)  Niniejsze rozporządzenie było poprzedzone rozporządzeniem Ministra Rozwoju i Finansów z dnia 15 grudnia 2016 r. w sprawie in-
formacji  zawartych  w  rocznym  sprawozdaniu  o  udzielonych  zamówieniach,  jego  wzoru  oraz  sposobu  przekazywania  (Dz.  U. 
poz. 2038), które traci moc z dniem wejścia w życie niniejszego rozporządzenia, zgodnie z art. 97 ustawy z dnia 11 września 2019 r. – 
Przepisy wprowadzające ustawę – Prawo zamówień publicznych (Dz. U. poz. 2020 oraz z 2020 r. poz. 1086 i 2275).

 
